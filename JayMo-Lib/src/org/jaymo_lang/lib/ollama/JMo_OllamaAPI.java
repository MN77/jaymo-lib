/*******************************************************************************
 * Copyright (C) 2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.ollama;

import java.net.HttpURLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.immute.datetime.JMo_DateTime;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.lib.json.Json;
import de.mn77.lib.json.JsonObject;


/**
 * @author Michael Nitsche
 * @created 2025-02-07
 *
 * @implNote
 *           curl -s http://localhost:11434/api/tags
 *           curl -s -X POST http://localhost:11434/api/generate -d "{\"model\":\"gemma2\", \"prompt\":\"Say hello\",
 *           \"stream\":false}"
 *
 * @implNote TODO
 * https://github.com/ollama/ollama/blob/main/docs/api.md
 * https://github.com/ollama/ollama/blob/main/docs/api.md?plain=1
 * https://www.postman.com/postman-student-programs/ollama-api/documentation/suc47x8/ollama-rest-api
 * https://ollama.com/blog/tool-support
 */
public class JMo_OllamaAPI extends A_Object {

	private final ArgCallBuffer argHost;
	private final ArgCallBuffer argPort;
	private String              host             = "localhost";
	private int                 port             = 11434;
	private String              model            = null;
	private int                 lastResponseCode = -1;


	public JMo_OllamaAPI() {
		this.argHost = null;
		this.argPort = null;
	}

	public JMo_OllamaAPI( final Call host ) {
		this.argHost = new ArgCallBuffer( 1, host );
		this.argPort = null;
	}

	public JMo_OllamaAPI( final Call host, final Call port ) {
		this.argHost = new ArgCallBuffer( 1, host );
		this.argPort = new ArgCallBuffer( 2, port );
	}


	@Override
	public void init( final CallRuntime cr ) {

		if( this.argHost != null ) {
			final JMo_Str s = this.argHost.init( cr, this, JMo_Str.class );
			this.host = s.rawString();
		}

		if( this.argPort != null ) {
			final JMo_Int p = this.argPort.init( cr, this, JMo_Int.class );
			this.port = p.rawInt( cr );
		}
	}


	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setModel":
				return this.mSetModel( cr );
			case "getModel":
				return this.mGetModel( cr );

			case "generate": // API
				return this.mGenerate( cr );
			case "prompt": // like generate, but simple result
				return this.mPrompt( cr );

			case "models":
			case "list": // ollama
			case "tags": // API
				return this.mListModels( cr );

			case "lastResponseCode":
				return this.mLastResponseCode( cr );

			default:
				return null;
		}
	}

	private String iFetch( final CallRuntime cr, final String target, final String requestBody ) {
		final R_OllamaResult response = U_Ollama.fetch( cr, this.host, this.port, target, requestBody );
		this.lastResponseCode = response.code();

		if( response.code() != HttpURLConnection.HTTP_OK )
			throw new RuntimeError( cr, response.data(), "" + response.code() );

		return response.data();
	}

	/**
	 * °generate(Str prompt)Map # Fetch full response.
	 */
	private JMo_Map mGenerate( final CallRuntime cr ) {
		final JMo_Str prompt = cr.arg( this, JMo_Str.class );
		final JsonObject jo = Json.createObject();

		if( this.model == null )
			this.model = U_Ollama.defaultModel( cr, this.host, this.port );

		jo.add( "model", this.model );
		jo.add( "prompt", prompt.rawString() );
		jo.add( "stream", false );
//		final String requestBody = "{\"model\":\"gemma2\", \"prompt\":\"Say hello\", \"stream\":false}";
//		MOut.print(jo.toString());

		final String response = this.iFetch( cr, "api/generate", jo.toString() );

		final SimpleList<I_Object> keys = new SimpleList<>();
		final SimpleList<I_Object> values = new SimpleList<>();

		try {
			final HashMap<?, ?> map = (HashMap<?, ?>)Json.parse( response );

			for( final Entry<?, ?> item : map.entrySet() ) {
				keys.add( new JMo_Str( (String)item.getKey() ) );
				values.add( Lib_Java.javaToJmo( item.getValue() ) );
			}
		}
		catch( final ParseException e ) {
			throw new ExternalError( cr, "Response parse error", e.getMessage() );
		}

		return new JMo_Map( keys, values );
	}

	/**
	 * °getModel()Str # Returns the current used model
	 */
	private I_Object mGetModel( final CallRuntime cr ) {
		cr.argsNone();
		return this.model == null
			? Nil.NIL
			: new JMo_Str( this.model );
	}

	/**
	 * °lastResponseCode()Int # Returns the response code of the last request.
	 */
	private I_Object mLastResponseCode( final CallRuntime cr ) {
		cr.argsNone();

		if( this.lastResponseCode == -1 )
			return Nil.NIL;
		else
			return new JMo_Int( this.lastResponseCode );
	}

	/**
	 * °models ^ tags
	 * °list ^ tags
	 * °tags()List # Fetch a list of all available models.
	 */
	private JMo_Table mListModels( final CallRuntime cr ) {
		cr.argsNone();
		final String response = this.iFetch( cr, "api/tags", null );
		final ArrayTable<I_Object> result = new ArrayTable<>( 4 );

		try {
			final HashMap<?, ?> map = (HashMap<?, ?>)Json.parse( response );
			final ArrayList<?> list = (ArrayList<?>)map.get( "models" );

			for( final Object o : list ) {
				final HashMap<?, ?> item = (HashMap<?, ?>)o;
				final String name = (String)item.get( "name" ); // "model"
				final MDateTime modified = new MDateTime(
					((String)item.get( "modified_at" )).substring( 0, 19 ).replace( 'T', ' ' ) );
				final long size = (Long)item.get( "size" );
				final String digest = (String)item.get( "digest" );

				result.addRow(
					new JMo_Str( name ),
					new JMo_DateTime( modified ),
					new JMo_Long( size ),
					new JMo_Str( digest ) );
			}
		}
		catch( final ParseException e ) {
			throw new ExternalError( cr, "Response parse error", e.getMessage() );
		}

		return new JMo_Table( result );
	}

	/**
	 * °prompt(Str prompt)Map # Fetch response.
	 */
	private JMo_Str mPrompt( final CallRuntime cr ) {
		final JMo_Str prompt = cr.arg( this, JMo_Str.class );
		final JsonObject jo = Json.createObject();

		if( this.model == null )
			this.model = U_Ollama.defaultModel( cr, this.host, this.port );

		jo.add( "model", this.model );
		jo.add( "prompt", prompt.rawString() );
		jo.add( "stream", false );

		final String response = this.iFetch( cr, "api/generate", jo.toString() );

		try {
			final HashMap<?, ?> map = (HashMap<?, ?>)Json.parse( response );
			final String result = (String)map.get( "response" );
			return new JMo_Str( result );
		}
		catch( final ParseException e ) {
			throw new ExternalError( cr, "Response parse error", e.getMessage() );
		}
	}

	/**
	 * °setModel(Str model)Same # Define the model to use
	 */
	private I_Object mSetModel( final CallRuntime cr ) {
		final JMo_Str arg = cr.arg( this, JMo_Str.class );
		this.model = arg.rawString();
		return this;
	}

}
