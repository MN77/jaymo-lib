/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.ntp;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.datetime.JMo_Date;
import org.jaymo_lang.object.immute.datetime.JMo_DateTime;
import org.jaymo_lang.object.immute.datetime.JMo_Time;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err_Network;
import de.mn77.lib.ntp.SNTP_Client;


/**
 * @author Michael Nitsche
 * @created 31.05.2022
 */
public class JMo_SntpClient extends A_ObjectSimple {

	private static final String DEFAULT = "pool.ntp.org";
	private final ArgCallBuffer arg;
	private SNTP_Client         client;


	public JMo_SntpClient() {
		this.arg = null;
	}

	public JMo_SntpClient( final Call server ) {
		this.arg = new ArgCallBuffer( 0, server );
	}

	@Override
	public void init( final CallRuntime cr ) {

		if( this.arg != null ) {
			final JMo_Str argStr = this.arg.init( cr, this, JMo_Str.class );
			final String address = Lib_Convert.toStr( cr, argStr ).rawString();
			this.client = new SNTP_Client( address );
		}
		else
			this.client = new SNTP_Client( JMo_SntpClient.DEFAULT );
	}

	/**
	 * °update()Same # Connect to Server and update time information
	 * °time()Time # Returns the corrected current time
	 * °date()Date # Returns the corrected current date
	 * °dateTime()DateTime # Returns the corrected current datetime
	 * °diff()Long # Returns the difference to local system time in milliseconds
	 */
	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "update":
				cr.argsNone();
				try {
					this.client.update();
				}
				catch( final Err_Network e ) {
					throw new ExternalError( cr, "NTP-Error", e.getMessage() );
				}
				return this;
			case "time":
				cr.argsNone();
				return new JMo_Time( this.client.getTime() );
			case "date":
				cr.argsNone();
				return new JMo_Date( this.client.getDate() );
			case "dateTime":
				cr.argsNone();
				return new JMo_DateTime( this.client.getDateTime() );
			case "diff":
				cr.argsNone();
				return new JMo_Long( this.client.getDifferenceMSek() );
		}

		return null;
	}

}
