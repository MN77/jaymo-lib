/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.emoji;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 2021-01-26
 *
 *          https://github.com/kcthota/emoji4j
 *
 *          https://www.w3schools.com/charsets/ref_emoji.asp
 *          http://www.iemoji.com/view/emoji/3/smileys-people/smiling-face-with-open-mouth
 *          https://www.w3schools.com/charsets/ref_emoji.asp
 *          https://www.w3schools.com/charsets/ref_emoji_smileys.asp
 */
public class JMo_Emoji extends A_ObjectSimple {

	/**
	 * ! Helper, to create Emoji-Chars
	 * + Emoji()
	 */
	public JMo_Emoji() {}

	/**
	 * °get(Int number...)Str # Create a Emoji by numbers.
	 * °smile()Str # Get Emoji: Smile
	 * °heart()Str # Get Emoji: Heart
	 * °hug ^ hugging
	 * °hugging()Str # Get Emoji: Hugging
	 * °laugh()Str # Get Emoji: Laugh
	 * °holy()Str # Get Emoji: Holy
	 * °wink()Str # Get Emoji: Wink
	 * °happy()Str # Get Emoji: Happy
	 * °tongue()Str # Get Emoji: Tongue
	 * °love()Str # Get Emoji: Love
	 * °cool()Str # Get Emoji: Cool
	 * °kiss()Str # Get Emoji: Kissing
	 */
	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "get":
				final I_IntNumber[] args = (I_IntNumber[])cr.argsVar( this, 1, I_IntNumber.class ); // TODO Only 2 arguments?
				final int[] ia = new int[args.length];
				for( int i = 0; i < args.length; i++ )
					ia[i] = Lib_Convert.toInt( cr, args[i] );
				return this.iEmoji( ia );

			case "smile":
				return this.iEmoji( 55357, 56835 );
//				return this.iEmoji(55357, 56836);
			case "heart":
				return this.iEmoji( 10084, 65039 );
			case "hug":
			case "hugging":
				return this.iEmoji( 55358, 56599 );
			case "laugh":
				return this.iEmoji( 55357, 56834 );
			case "holy":
				return this.iEmoji( 55357, 56839 );
			case "wink":
				return this.iEmoji( 55357, 56841 );
			case "happy":
				return this.iEmoji( 55357, 56842 );
			case "tongue":
				return this.iEmoji( 55357, 56843 );
			case "love":
				return this.iEmoji( 55357, 56845 );
			case "cool":
				return this.iEmoji( 55357, 56846 );
			case "kiss":
				return this.iEmoji( 55357, 56856 );


			default:
				return null;
		}
	}

	private I_Object iEmoji( final int... chars ) {
		String result = "";
		for( final int c : chars )
			result += (char)c;

		return new JMo_Str( result );
	}

}
