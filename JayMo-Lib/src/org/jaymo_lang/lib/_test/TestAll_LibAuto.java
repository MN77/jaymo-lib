/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jmo-lang.org>.
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib._test;

import org.jaymo_lang._test.util.AutoTest;
import org.jaymo_lang._test.util.FAIL_ACTION;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public class TestAll_LibAuto {

	private static final FAIL_ACTION FAIL       = FAIL_ACTION.SHOW;
	private static final DEBUG_MODE  DEBUG      = DEBUG_MODE.NO;
	private static final boolean     PARSE_ONLY = false;


	public static void main( final String[] args ) {
		MOut.setDebug( TestAll_LibAuto.DEBUG );

		try {
			final String testdir = "/home/mike/Prog/JayMo/lib/tests_auto";
			AutoTest.testAll( TestAll_LibAuto.FAIL, TestAll_LibAuto.DEBUG, TestAll_LibAuto.PARSE_ONLY, testdir );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
