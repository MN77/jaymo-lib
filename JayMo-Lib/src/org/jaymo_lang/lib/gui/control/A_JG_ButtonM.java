/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 01.08.2023
 */
public abstract class A_JG_ButtonM implements I_JG_Methods {

	protected abstract I_Object getImage();
	protected abstract String getText();
	protected abstract void setImage( JMo_Image image );
	protected abstract void setText( String text );


	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {

		switch( method ) {
			case "setText":
				final String text = cr.arg( obj, JMo_Str.class ).rawString();
				this.setText( text );
				return obj;
			case "getText":
				cr.argsNone();
				return new JMo_Str( this.getText() ); // String will be maybe empty
			case "setImage":
//				final JMo_Image image = (JMo_Image)cr.args( this, JMo_Image.class )[0];
				final I_Object image = cr.argExt( obj, JMo_Image.class, Nil.class );
				final JMo_Image image2 = image == Nil.NIL
					? null
					: (JMo_Image)image;
				this.setImage( image2 );
				return obj;
			case "getImage":
				cr.argsNone();
				return this.getImage();
		}

		return null;
	}

}
