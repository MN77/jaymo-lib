/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll.composit;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Range;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 24.11.2023
 */
public abstract class A_JG_SpinnerM implements I_JG_Methods {

	protected abstract void setValue( int value );
	protected abstract int getValue();
	protected abstract void setMinMax( final int min, final int max );
	protected abstract int getMin();
	protected abstract int getMax();

	/*
	 * °get ^ getvalue
	 * °getValue()Int # Returns the current value
	 * °getMin()Int # Returns the minimum limit
	 * °getMax()Int # Returns the maximum limit
	 * °set ^ setvalue
	 * °setValue(Int value)Same # Set value
 	 * °setRange(Range r)Same # Set the minimum and maximum
 	 * °setRange(Int Range r)Same # Set the minimum and maximum
	 */
	@Override
	public final I_Object call( final CallRuntime cr, final String method, final I_Object obj ) {

		switch( method ) {
			case "set":
			case "setValue":
				final JMo_Int arg = cr.arg( obj, JMo_Int.class );
				final int value = arg.rawInt( cr );
				Lib_Error.ifNotBetween( cr, this.getMin(), this.getMax(), value, "Value" );
				this.setValue( value );
				return obj;
			case "get":
			case "getValue":
				cr.argsNone();
				return new JMo_Int( this.getValue() );
			case "getMin":
			case "getMinimum":
				cr.argsNone();
				return new JMo_Int( this.getMin() );
			case "getMax":
			case "getMaximum":
				cr.argsNone();
				return new JMo_Int( this.getMax() );
			case "setRange":
				final JMo_Range range = cr.arg( obj, JMo_Range.class );
				final Group2<I_Object, I_Object> rangeValues = range.getInternalValues();
				this.iSetMinMax( cr, rangeValues.o1, rangeValues.o2 );
				return obj;
			case "setMinMax":
				final I_Object[] mmArgs = cr.args( obj, JMo_Int.class, JMo_Int.class );
				this.iSetMinMax( cr, mmArgs[0], mmArgs[1] );
				return obj;
		}

		return null;
	}

	private void iSetMinMax( final CallRuntime cr, final I_Object min, final I_Object max ) {
		final int iMin = Lib_Convert.toInt( cr, min );
		final int iMax = Lib_Convert.toInt( cr, max );
		Lib_Error.ifTooSmall( cr, iMin, iMax ); // min+1, max
		this.setMinMax( iMin, iMax );
	}

}
