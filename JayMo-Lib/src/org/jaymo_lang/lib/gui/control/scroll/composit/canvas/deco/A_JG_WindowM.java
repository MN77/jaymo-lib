/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll.composit.canvas.deco;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 28.09.2024
 */
public abstract class A_JG_WindowM implements I_JG_Methods {

	protected abstract void add( I_Object obj );
	protected abstract void close(final CallRuntime cr );
	protected abstract void setSize( int width, int height );
	protected abstract String getTitle();
	protected abstract void setTitle( String title );


	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {
		switch( method ) {
//			case "+"
			case "add":
				final I_Object o = cr.args( obj, I_Object.class )[0]; // Only 1 is allowed! For more, use a Layout!
				this.add(o);
				return obj;
			case "setSize":
				final I_Object[] args = cr.args( obj, I_IntNumber.class, I_IntNumber.class );
				int dx = Lib_Convert.toInt( cr, args[0] );
				int dy = Lib_Convert.toInt( cr, args[1] );

				if( dx < 1 || dy < 1 ) {
					cr.warning( "Value out of bounds", "Minimum of 1 useful, got " + dx + "/" + dy );
					dx = Math.max( dx, 1 );
					dy = Math.max( dy, 1 );
				}

				this.setSize(dx, dy);
				return obj;
			case "getTitle":
				cr.argsNone();
				return new JMo_Str( this.getTitle() );
			case "setTitle":
				final JMo_Str oTitle = cr.arg( obj, JMo_Str.class );
//				final String title = Lib_Convert.toStr( cr2, cr2.args( this, JMo_Str.class )[0] ).rawString();
				String title = oTitle.rawString();
				this.setTitle(title);
//				this.gHauptfenster().sTitel(sTitle);	// TODO Automatische Prüfung ob schon erzeugt!
				return obj;
//			case "dispose":
//			case "stop":
			case "close":
				cr.argsNone();
				this.close( cr );
				return obj;
		}

		return null;
	}

}
