/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control.scroll.composit;

import java.util.List;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 25.11.2023
 */
public abstract class A_JG_TableM implements I_JG_Methods {

	public abstract void setTable( CallRuntime cr, JMo_Table items );
//	public abstract JMo_Table getSelection();
	public abstract I_Object getSelectionRow(); // List|Nil
	public abstract JMo_Table getSelectionTable();
	public abstract void selectRows(final CallRuntime cr, int[] ia);
	public abstract void selectRow(final CallRuntime cr, int i);
	public abstract void selectRange(final CallRuntime cr, int from, int to);
	public abstract void scrollTop();
	public abstract void selectAll(CallRuntime cr);
	public abstract void selectNone();


	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object that ) {
		switch( method ) {
			case "set":
			case "setData":
//			case "setTable": // TODO
				JMo_Table table = cr.arg( that, JMo_Table.class );
				this.setTable(cr, table);
				return that;
//			case "selection":
//				return this.mSelection( cr );
			case "selectionTable":
				cr.argsNone();
				return this.getSelectionTable();
			case "selectionRow":
				cr.argsNone();
				return this.getSelectionRow();

			case "selectRow":
				I_IntNumber arg = cr.arg( that, I_IntNumber.class );
				final int index = Lib_Convert.toInt( cr, arg ) - 1;
				this.selectRow(cr, index);

			case "selectRows":
				final I_Object[] selectRowArgs = cr.argsFlex( that, 1, 2 );

				if( selectRowArgs.length == 1 ) {
					cr.argType( selectRowArgs[0], JMo_List.class );
					final List<I_Object> positions = ((JMo_List)selectRowArgs[0]).getInternalCollection();
					int[] ia = new int[positions.size()];

					for(int i=0; i<positions.size(); i++)
						ia[i] = Lib_Convert.toInt( cr, cr.argType( positions.get( i ), I_IntNumber.class ) ) - 1;

					this.selectRows(cr, ia);
				}
				else {
					final int from = Lib_Convert.toInt( cr, cr.argType( selectRowArgs[0], I_IntNumber.class ) ) - 1;
					final int to = Lib_Convert.toInt( cr, cr.argType( selectRowArgs[1], I_IntNumber.class ) ) - 1;
					this.selectRange( cr, from, to );
				}

				return that;

			case "selectAll":
				cr.argsNone();
				this.selectAll(cr);
				return that;
			case "selectNone":
				cr.argsNone();
				this.selectNone();
				return that;

			case "scrollTop":
				cr.argsNone();
				this.scrollTop();
				return that;
		}

		return null;
	}

}
