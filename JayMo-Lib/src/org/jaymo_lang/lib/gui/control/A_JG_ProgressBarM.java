/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Range;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.10.2023
 */
public abstract class A_JG_ProgressBarM implements I_JG_Methods {

	protected abstract void setRange( int min, int max );
	protected abstract void setValue( int value );
	protected abstract int getMinimum();
	protected abstract int getMaximum();
	protected abstract int getValue();

	/*
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 * °setValue(IntNumber num)Same # Set the value of the progress bar.
	 * °getMin()Int # Returns the current value.
	 * °getMax()Int # Returns the current value.
	 * °getValue()Int # Returns the current value.
	 * °getRange()Range # Returns the current range from min to max.
	 * °next ^ inc
	 * °inc()Same # Push the value one step forward
	 * °hasNext()Bool # Returns true, if value lower than max.
	 */
	@Override
	public final I_Object call( final CallRuntime cr, final String method, I_Object obj ) {

		switch( method ) {
			case "setRange":
				final I_Object[] args = cr.argsFlex( obj, 1, 2 );

				int min = 0;
				int max = 0;

				if( args.length == 1 ) {
					final JMo_Range range = cr.argType( args[0], JMo_Range.class );
					min = Lib_Convert.toInt( cr, range.getInternalValues().o1 );
					max = Lib_Convert.toInt( cr, range.getInternalValues().o2 );
				}
				else {
					min = Lib_Convert.toInt( cr, args[0] );
					max = Lib_Convert.toInt( cr, args[1] );
				}

				Err.ifTooSmall( 0, min );
				Err.ifToBig( max - 1, min );

				this.setRange( min, max );
				return obj;
			case "setValue":
				final JMo_Int arg = cr.arg( obj, JMo_Int.class );
				final int value = arg.rawInt( cr );
				Err.ifOutOfBounds( this.getMinimum(), this.getMaximum(), value );
				this.setValue( value );
				return obj;
//			case "setMin":
//				return this.mSetMin(cr);
//			case "setMax":
//				return this.mSetMax(cr);
			case "getMin":
				cr.argsNone();
				return new JMo_Int( this.getMinimum() );
			case "getMax":
				cr.argsNone();
				return new JMo_Int( this.getMaximum() );
			case "getValue":
				cr.argsNone();
				return new JMo_Int( this.getValue() );
			case "getRange":
				cr.argsNone();
				return JMo_Range.createNew( cr, new JMo_Int( this.getMinimum() ), new JMo_Int( this.getMaximum() ) );

			// case "step": // TODO
			case "next": // TODO
			case "inc":  // TODO
				cr.argsNone();
				final int max2 = this.getMaximum();
				final int newValue = this.getValue() + 1;

				if( newValue <= max2 ) {
					this.setValue( newValue );
					return new JMo_Int( newValue );
				}
				else
					return new JMo_Int( max2 );
			case "hasNext":
				cr.argsNone();
				return JMo_Bool.getObject( this.getValue() < this.getMaximum() );
		}

		return null;
	}

}
