/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.gui.control;

import org.jaymo_lang.lib.font.JMo_Font;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.I_JG_Methods;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 14.05.2024
 */
public abstract class A_JG_ToggleButtonM implements I_JG_Methods {

	@Override
	public final I_Object call( final CallRuntime cr, final String method, final I_Object obj ) {

		switch( method ) {
			case "setText":
				final I_Atomic o = (I_Atomic)cr.args( obj, I_Atomic.class )[0];
				final String text = Lib_Convert.toStr( cr, o ).rawString();
				this.setText( text );
				return obj;
			case "getText":
				cr.argsNone();
				return new JMo_Str( this.getText() );

			case "setFont":
				final JMo_Font font = cr.arg( obj, JMo_Font.class );
				this.setFont( font );
				return obj;

			case "isSelected":
//			case "getSelection":
				cr.argsNone();
				return JMo_Bool.getObject( this.isSelected() );

			case "setSelected":
				final boolean b = Lib_Convert.toBoolean( cr, cr.args( obj, JMo_Bool.class )[0] );
				this.setSelected( b );
				return obj;

			case "setImage":
			case "setIcon":
				final I_Object img = cr.argExt( obj, JMo_Image.class, Nil.class );
				if( img == Nil.NIL )
					this.removeImage();
				else
					this.setImage( (JMo_Image)img );
				return obj;
		}

		return null;
	}

	//	protected abstract I_Object getImage();
	protected abstract String getText();
	protected abstract boolean isSelected();
	protected abstract void removeImage();
	protected abstract void setFont( JMo_Font font );
	protected abstract void setImage( JMo_Image image );
	protected abstract void setSelected( boolean b );


	protected abstract void setText( String text );

}
