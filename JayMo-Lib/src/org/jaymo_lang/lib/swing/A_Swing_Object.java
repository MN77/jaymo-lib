/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing;

import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

/**
 * @author Michael Nitsche
 * @created 06.04.2020
 */
public abstract class A_Swing_Object extends A_EventObject implements I_Swing_Object {

	@Override
	public final void init(CallRuntime cr) {
		this.init1(cr);
	}

	protected abstract void init1( CallRuntime cr );


	@Override
	protected final I_Object callMethod( final CallRuntime cr, final String method ) {
		return this.callMethod1( cr, method );
	}

	protected abstract I_Object callMethod1( CallRuntime cr, String method );

}
