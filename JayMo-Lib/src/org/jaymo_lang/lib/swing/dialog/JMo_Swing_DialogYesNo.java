/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.dialog;

import java.awt.Component;

import org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco.JMo_Swing_Main;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.container.SyncBox;
import de.mn77.lib.swing.DIALOG_TYPE;
import de.mn77.lib.swing.Lib_SwingDialog;


/**
 * @author Michael Nitsche
 * @created 24.03.2022
 */
public class JMo_Swing_DialogYesNo extends A_EventObject {

	private static final String ENUM_INFO     = "INFO";
	private static final String ENUM_ERROR    = "ERROR";
	private static final String ENUM_WARNING  = "WARNING";
	private static final String ENUM_QUESTION = "QUESTION";
	private final ArgCallBuffer argParent;
	private final ArgCallBuffer argTitle;
	private final ArgCallBuffer argMsg;
	private JMo_Enum            type          = null;


	public JMo_Swing_DialogYesNo( final Call parent, final Call title, final Call msg ) {
		this.argParent = new ArgCallBuffer( 0, parent );
		this.argTitle = new ArgCallBuffer( 1, title );
		this.argMsg = new ArgCallBuffer( 2, msg );
	}

	/**
	 * °INFO()Enum # Dialog type is 'Info'
	 * °ERROR()Enum # Dialog type is 'Error'
	 * °WARNING()Enum # Dialog type is 'Warning'
	 * °QUESTION()Enum # Dialog type is 'Question'
	 */
	@Override
	public A_Immutable getConstant( final CallRuntime cr, final String name ) {

		switch( name ) {
			case ENUM_INFO:
				return new JMo_Enum( this.getTypeName(), JMo_Swing_DialogYesNo.ENUM_INFO );
			case ENUM_WARNING:
				return new JMo_Enum( this.getTypeName(), JMo_Swing_DialogYesNo.ENUM_WARNING );
			case ENUM_ERROR:
				return new JMo_Enum( this.getTypeName(), JMo_Swing_DialogYesNo.ENUM_ERROR );
			case ENUM_QUESTION:
				return new JMo_Enum( this.getTypeName(), JMo_Swing_DialogYesNo.ENUM_QUESTION );
			default:
				return null;
		}
	}

	@Override
	public void init( final CallRuntime cr ) {
		this.argParent.init( cr, this, JMo_Swing_Main.class ); // TODO Frame?
		this.argTitle.init( cr, this, JMo_Str.class );
		this.argMsg.init( cr, this, JMo_Str.class );
	}

	@Override
	public boolean validateEvent( final String event ) {
		return false;
	}

	/**
	 * °setType(Enum type)Same # Set the type of the dialog
	 */
	@Override
	protected I_Object callMethod( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "show":
				return this.mShow( cr );
			case "setType":
				this.type = cr.argEnum( this );
				return this;
		}

		return null;
	}

	/**
	 * °show()Same # Show the dialog and block the current thread
	 */
	private JMo_Bool mShow( final CallRuntime cr ) {
		cr.argsNone();
		final String title = Lib_Convert.toStr( cr, this.argTitle.get() ).rawString();
		final String msg = Lib_Convert.toStr( cr, this.argMsg.get() ).rawString();
		final Component parent = ((JMo_Swing_Main)this.argParent.get()).getSwing();

		DIALOG_TYPE type = DIALOG_TYPE.INFO;

		if( this.type != null )
			if( this.type.getName().equals( JMo_Swing_DialogYesNo.ENUM_WARNING ) )
				type = DIALOG_TYPE.WARNING;
			else if( this.type.getName().equals( JMo_Swing_DialogYesNo.ENUM_ERROR ) )
				type = DIALOG_TYPE.ERROR;
			else if( this.type.getName().equals( JMo_Swing_DialogYesNo.ENUM_QUESTION ) )
				type = DIALOG_TYPE.QUESTION;

//		Lib_SwingDialog.yesOrNo(null, null, this.getTypeName(), this.getTypeName(), null);
		final SyncBox<Boolean> result = new SyncBox<>();
		Lib_SwingDialog.yesOrNo( parent, type, title, msg, ( final Boolean b ) -> {
			result.set( b );
		} );

		return JMo_Bool.getObject( result.get() );
	}

}
