/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import java.awt.Component;
import java.awt.Container;

import javax.swing.SpringLayout;

import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 */
public class JMo_Swing_LayoutFlex extends A_Swing_Object implements I_Swing_Object { // TODO ?!? A_Swing_JComponent and JPanel?

	private final Container    container     = new Container();
	private final SpringLayout layout        = new SpringLayout();
	private int[]              borders_trbl  = null;
	private boolean            hasComponents = false;


	@Override
	public I_Object callMethod1( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setBorder":
				this.mSetBorder( cr );
				return this;
			case "put":
				return this.mPut( cr );

			default:
				return null;
		}
	}

	public Component getSwing() {
		return this.container;
	}

	/** @override **/
	@Override
	public void init1( final CallRuntime cr ) {
		this.container.setLayout( this.layout );
//		this.container.setBackground(Color.yellow);
	}

	@Override
	public boolean validateEvent( final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °put(Swing_Object obj)FlexItem # Put an object to this layout. The returned FlexItem is to set the position.
	 */
	private Swing_FlexItem mPut( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_Swing_Object.class )[0];
		this.hasComponents = true;
		final Component soc = ((I_Swing_Object)cr.args( arg, I_Swing_Object.class )[0]).getSwing();
		this.container.add( soc );
		return new Swing_FlexItem( this.container, this.layout, soc, this.borders_trbl );
	}

	/**
	 * °setBorder(Int all)Same # Set border margins.
	 * °setBorder(Int top_bottom, Int left_right)Same # Set border margins.
	 * °setBorder(Int top, Int left_right, Int bottom)Same # Set border margins.
	 * °setBorder(Int top, Int right, Int bottom, Int left)Same # Set border margins.
	 */
	private void mSetBorder( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 4 );
		if( this.hasComponents )
			cr.warning( "Components have already been added!", "Border must be set before all 'puts'!" );
		else
			this.borders_trbl = Lib_Layout.border( cr, args );
	}

}
