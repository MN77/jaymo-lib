/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JPanel;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;


/**
 * @author Michael Nitsche
 */
public class JMo_Swing_LayoutGrid extends A_Swing_JComponent {

//	private final Call[] k;
	private final JPanel        container = new JPanel();
	private GridLayout          grid;
	private final ArgCallBuffer par_dx, par_dy;
	private int                 calc_dx, calc_dy;
	private int                 itemcount = 0;


//	public SwingGroupBorder(Call... k) {
//		this.k=k;
//	}


	public JMo_Swing_LayoutGrid( final Call dx, final Call dy ) {
		this.par_dx = new ArgCallBuffer( 0, dx );
		this.par_dy = new ArgCallBuffer( 1, dy );
	}


	// ERZEUGEN

	/**
	 * °setColumns(IntNumber amount)Same # Set the amount of columns.
	 * °setRows(IntNumber amount)Same # Set the amount of rows.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				return this.add( cr );
			case "setColumns":
				if( this.calc_dy > 0 )
					throw new RuntimeError( cr, "Layout resize error", "Rows are not 0" );
				final int cols = Lib_Convert.toInt( cr, cr.args( this, I_IntNumber.class )[0] );
				this.grid.setColumns( cols );
				this.container.doLayout();
				return this;
			case "setRows":
				if( this.calc_dx > 0 )
					throw new RuntimeError( cr, "Layout resize error", "Columns are not 0" );
				final int rows = Lib_Convert.toInt( cr, cr.args( this, I_IntNumber.class )[0] );
				this.grid.setRows( rows );
				this.container.doLayout();
				return this;

			default:
				return null;
		}
	}

	public Component getSwing() {
		return this.container;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.calc_dx = Lib_Convert.toInt( cr, this.par_dx.init( cr, this, I_IntNumber.class ) );
		this.calc_dy = Lib_Convert.toInt( cr, this.par_dy.init( cr, this, I_IntNumber.class ) );

		Lib_Error.ifTooSmall( cr, 0, this.calc_dx );
		Lib_Error.ifTooSmall( cr, 0, this.calc_dy );
		if( this.calc_dx == 0 && this.calc_dy == 0 )
			throw new RuntimeError( cr, "Invalid parameters", "Either X or Y can be 0, but not both!" );

		this.grid = new GridLayout( this.calc_dy, this.calc_dx );
		this.container.setLayout( this.grid ); // Ein Wert darf 0 sein.

//		this.container.setSize(100, 200);
//		this.container.setPreferredSize(new Dimension(200,300));
//		for(Call call : this.k) {
//			S_Object o = call.exec(this);
//			if(!(o instanceof S_SwingObj))
//				throw new ErrorExec("This is not a Swing-Object", Lib_Konvert.typeName(o.getType()), c);
//			S_SwingObj b=(S_SwingObj)o;
//			b.callInit(c);
//			this.container.add(b.gSwing());
////			c.layout();	// this.swing()
//		}
	}

	@Override
	public boolean validateEvent2(final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

//	private SwingGroupGrid put(Call c) {
//		S_Object[] o=Lib_Par.checkPars(c, this, S_SwingObj.class, Int.class, Int.class);
//		S_SwingObj so=(S_SwingObj)Lib_Konvert.getValue(o[0], c);
//		int x=Lib_Konvert.getIntValue(o[1], c);
//		int y=Lib_Konvert.getIntValue(o[2], c);
//
//		GridBagConstraints gbc = new GridBagConstraints();
//		gbc.fill=GridBagConstraints.HORIZONTAL;	// .BOTH
//		gbc.gridx=x;
//		gbc.gridy=y;
//		gbc.anchor=GridBagConstraints.NORTHWEST;
//
//		this.container.add(so.gSwing(), gbc);
//
////		//Lib_Konvert
//////	      button.setForeground(Color.blue);
////		button.setText(((Obj_String)o).gValue());
//		return this;
//	}

	/**
	 * °add(Swing_Object objects...)Same # Add objects to this layout.
	 */
	private JMo_Swing_LayoutGrid add( final CallRuntime cr ) {
		final I_Swing_Object[] args = (I_Swing_Object[])cr.argsVar( this, 1, I_Swing_Object.class );

		for( final I_Swing_Object so : args ) {
			if( this.calc_dx > 0 && this.calc_dy > 0 ) {
				if( this.itemcount >= this.calc_dx * this.calc_dy )
					throw new RuntimeError( cr, "Too much Items", "" + this.calc_dx + " x " + this.calc_dy + " = " + this.calc_dx * this.calc_dy + " Max! Got " + (this.itemcount + 1) );
				this.itemcount++;
			}

			this.container.add( so.getSwing() );
		}
		return this;
	}

}
