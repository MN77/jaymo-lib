/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.SpringLayout;

import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 */
public class Swing_FlexItem extends A_Swing_Object {

	private enum ANCHOR {
		L,
		R,
		T,
		B
	}


	private final Container    container;
	private final SpringLayout layout;
	private final Component    component;


	private final int[] borders_trbl;


	public Swing_FlexItem( final Container container, final SpringLayout layout, final Component component, final int[] borders_trbl ) {
		this.container = container;
		this.layout = layout;
		this.component = component;
		this.borders_trbl = borders_trbl;
	}

	/**
	 * TODO setSize, setPos, set... ?!?
	 */
	@Override
	public I_Object callMethod1( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "left":
				return this.anchor( cr, ANCHOR.L );
			case "right":
				return this.anchor( cr, ANCHOR.R );
			case "top":
				return this.anchor( cr, ANCHOR.T );
			case "bottom":
				return this.anchor( cr, ANCHOR.B );
//			case "leftTop":		result=anchor(c, ANCHOR.L, ANCHOR.T); break;
//			case "rightBottom":	result=anchor(c, ANCHOR.R, ANCHOR.B); break;
			case "pos": // Top, Right, Bottom, Left
				return this.pos( cr );

			case "size":
				return this.size( cr );
			case "width":
				return this.width( cr );
			case "height":
				return this.height( cr );

			default:
				return null;
		}
	}

	public Component getSwing() {
		return null;
	}

	@Override
	public void init1( final CallRuntime cr ) {}

	@Override
	public boolean validateEvent( final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °left()Same # Anchor the component to the left.
	 * °left(IntNumber pixel)Same # Anchor the component to the left.
	 * °left(Swing_Object obj)Same # Anchor the component to the left.
	 * °left(Swing_Object obj, IntNumber pixel)Same # Anchor the component to the left.
	 *
	 * °right()Same # Anchor the component to the right.
	 * °right(IntNumber pixel)Same # Anchor the component to the right.
	 * °right(Swing_Object obj)Same # Anchor the component to the right.
	 * °right(Swing_Object obj, IntNumber pixel)Same # Anchor the component to the right.
	 *
	 * °top()Same # Anchor the component to the top.
	 * °top(IntNumber pixel)Same # Anchor the component to the top.
	 * °top(Swing_Object obj)Same # Anchor the component to the top.
	 * °top(Swing_Object obj, IntNumber pixel)Same # Anchor the component to the top.
	 *
	 * °bottom()Same # Anchor the component to the bottom.
	 * °bottom(IntNumber pixel)Same # Anchor the component to the bottom.
	 * °bottom(Swing_Object obj)Same # Anchor the component to the bottom.
	 * °bottom(Swing_Object obj, IntNumber pixel)Same # Anchor the component to the bottom.
	 */
	private Swing_FlexItem anchor( final CallRuntime cr, final ANCHOR a ) {
		final I_Object[] args = cr.argsFlex( this, 0, 2 );
		final int parlen = args.length;

		Component target = this.container;
		int pad = 0;

		if( parlen == 2 ) {
			final I_Swing_Object swo = cr.argType( args[0], I_Swing_Object.class );
			target = swo.getSwing();
			pad = Lib_Convert.toInt( cr, cr.argType( args[1], I_IntNumber.class ) );
		}

		if( parlen == 1 ) {
			final I_Object arg = cr.argTypeExt( args[0], new Class[]{ I_Swing_Object.class, I_IntNumber.class } );
			if( arg instanceof I_Swing_Object )
				target = ((I_Swing_Object)arg).getSwing();
			else
				pad = Lib_Convert.toInt( cr, arg );
		}

		this.iAnchorSet( a, target, pad );
		return this;
	}

	/**
	 * °height(Int height)Same # Set the height of the component.
	 */
	private Swing_FlexItem height( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Int.class )[0];
		final int h = Lib_Convert.toInt( cr, arg );
		final Dimension d = this.component.getPreferredSize();
		d.height = h;
		this.component.setPreferredSize( d );
		return this;
	}

	private void iAnchorSet( final ANCHOR a, final Component target, int pad ) {
		String targetside = null;

		switch( a ) {
			case L:
				targetside = target == this.container ? SpringLayout.WEST : SpringLayout.EAST;
				break;
			case R:
				targetside = target == this.container ? SpringLayout.EAST : SpringLayout.WEST;
				break;
			case T:
				targetside = target == this.container ? SpringLayout.NORTH : SpringLayout.SOUTH;
				break;
			case B:
				targetside = target == this.container ? SpringLayout.SOUTH : SpringLayout.NORTH;
				break;
		}

		// borders
		if( this.borders_trbl != null && target == this.container ) {
			if( a == ANCHOR.T )
				pad += this.borders_trbl[0];
			if( a == ANCHOR.R )
				pad += this.borders_trbl[1];
			if( a == ANCHOR.B )
				pad += this.borders_trbl[2];
			if( a == ANCHOR.L )
				pad += this.borders_trbl[3];
		}

		switch( a ) {
			case L:
				this.layout.putConstraint( SpringLayout.WEST, this.component, pad, targetside, target );
				break;
			case R:
				this.layout.putConstraint( SpringLayout.EAST, this.component, -pad, targetside, target );
				break;
			case T:
				this.layout.putConstraint( SpringLayout.NORTH, this.component, pad, targetside, target );
				break;
			case B:
				this.layout.putConstraint( SpringLayout.SOUTH, this.component, -pad, targetside, target );
				break;
		}
	}

	private void iPos( final CallRuntime cr, final ANCHOR a, final I_Object o ) {
		Component target = this.container;
		int pad = 0;

		if( o != Nil.NIL ) {
			if( o instanceof I_Swing_Object )
				target = ((I_Swing_Object)o).getSwing();
			else
				pad = Lib_Convert.toInt( cr, o );

			this.iAnchorSet( a, target, pad );
		}
	}

	/**
	 * °pos(Swing_Object|IntNumber top, Swing_Object|IntNumber right, Swing_Object|IntNumber bottom, Swing_Object|IntNumber left)Same # Set the position of the component.
	 */
	private Swing_FlexItem pos( final CallRuntime cr ) {
		final Class<?>[] so_in = new Class[]{ I_Swing_Object.class, I_IntNumber.class };
		final I_Object[] args = cr.argsExt( this, so_in, so_in, so_in, so_in );

		this.iPos( cr, ANCHOR.T, args[0] );
		this.iPos( cr, ANCHOR.R, args[1] );
		this.iPos( cr, ANCHOR.B, args[2] );
		this.iPos( cr, ANCHOR.L, args[3] );

		return this;
	}

	/**
	 * °size(Int width, Int height)Same # Set the size of the component.
	 */
	private Swing_FlexItem size( final CallRuntime cr ) {
		final I_Object[] o = cr.args( this, JMo_Int.class, JMo_Int.class );
		final int w = Lib_Convert.toInt( cr, o[0] );
		final int h = Lib_Convert.toInt( cr, o[1] );
		this.component.setPreferredSize( new Dimension( w, h ) );
		return this;
	}

	/**
	 * °width(Int width)Same # Set the width of the component.
	 */
	private Swing_FlexItem width( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Int.class )[0];
		final int w = Lib_Convert.toInt( cr, arg );
		final Dimension d = this.component.getPreferredSize();
//		d.height = 10;
		d.width = w;
		this.component.setPreferredSize( d );
		return this;
	}

}
