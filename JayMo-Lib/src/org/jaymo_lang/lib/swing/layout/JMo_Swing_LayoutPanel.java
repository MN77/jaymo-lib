/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.jaymo_lang.lib.graphic.A_ColorModel;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.constant.position.POSITION;


/**
 * @author Michael Nitsche
 * @created 2021-01-07
 *
 *          https://docs.oracle.com/javase/tutorial/uiswing/components/border.html
 *          http://www.java2s.com/Code/Java/Swing-JFC/Borderofallkinds.htm
 */
public class JMo_Swing_LayoutPanel extends A_Swing_JComponent implements I_Swing_Object {

	private enum SIMPLE {
		ETCHED_RAISED,
		ETCHED_LOWERED,
		BEVEL_RAISED,
		BEVEL_LOWERED
	}


	private final JPanel panel = new JPanel();


	private Border   border   = null;
	private String   title    = null;
	private POSITION titlePos = null;


	/**
	 * +Swing_LayoutPanel()
	 */
	public JMo_Swing_LayoutPanel() {}

	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				this.add( cr );
				return this;

			case "borderEmpty":
			case "borderMargin":
				this.borderEmpty( cr );
				return this;
			case "borderLine":
				this.borderLine( cr );
				return this;
			case "borderRaisedEtched":
			case "borderEtchedRaised":
				this.borderSimple( cr, SIMPLE.ETCHED_RAISED );
				return this;
			case "borderLoweredEtched":
			case "borderEtchedLowered":
				this.borderSimple( cr, SIMPLE.ETCHED_LOWERED );
				return this;
			case "borderRaisedBevel":
			case "borderBevelRaised":
				this.borderSimple( cr, SIMPLE.BEVEL_RAISED );
				return this;
			case "borderLoweredBevel":
			case "borderBevelLowered":
				this.borderSimple( cr, SIMPLE.BEVEL_LOWERED );
				return this;

			case "setTitle":
				this.setTitle( cr );
				return this;

			default:
				return null;
		}
	}

	public Component getSwing() {
		return this.panel;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		final BorderLayout layout = new BorderLayout();
		this.panel.setLayout( layout );
	}

	@Override
	public boolean validateEvent2( final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °add(Swing_Object obj)Same # Add the object to this panel.
	 */
	private void add( final CallRuntime cr ) {
		final I_Swing_Object arg = (I_Swing_Object)cr.args( this, I_Swing_Object.class )[0];
		// TODO Können hier mehrere hinzugefügt werden? Nur eins erlauben? Dies direkt oder alternativ als Argument an den Typ übergeben?
		this.panel.add( arg.getSwing(), BorderLayout.CENTER );
	}

	/**
	 * °borderEmpty ^ borderMargin
	 * °borderMargin(Int all)Same # Set border margins.
	 * °borderMargin(Int top_bottom, Int left_right)Same # Set border margins.
	 * °borderMargin(Int top, Int left_right, Int bottom)Same # Set border margins.
	 * °borderMargin(Int top, Int right, Int bottom, Int left)Same # Set border margins.
	 */
	private void borderEmpty( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 4 );
		final int[] trbl = Lib_Layout.border( cr, args );
		this.border = BorderFactory.createEmptyBorder( trbl[0], trbl[3], trbl[2], trbl[1] );
		this.iSet();
	}

	/**
	 * °borderLine()Same # Set border style: Line
	 * °borderLine(ColorSpace color)Same # Set border style: Line
	 * °borderLine(ColorSpace color, IntNumber width)Same # Set border style: Line
	 * °borderLine(ColorSpace color, IntNumber width, Bool rounded)Same # Set border style: Line
	 */
	private void borderLine( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 0, 3 );
		Color color = Color.black;
		int thickness = 1;
		boolean round = false;

		if( args.length >= 1 )
			color = cr.argType( args[0], A_ColorModel.class ).getColor();
		if( args.length >= 2 )
			thickness = Lib_Convert.toInt( cr, cr.argType( args[1], I_IntNumber.class ) );
		if( args.length == 3 )
			round = Lib_Convert.toBoolean( cr, cr.argType( args[2], JMo_Bool.class ) );

		this.border = BorderFactory.createLineBorder( color, thickness, round );
		this.iSet();
	}

	/**
	 * °borderRaisedEtched ^ borderEtchedRaised
	 * °borderEtchedRaised()Same # Set border style: Etched raised
	 * °borderLoweredEtched ^ borderEtchedLowered
	 * °borderEtchedLowered()Same # Set border style: Etched lowered
	 * °borderRaisedBevel ^ borderBevelRaised
	 * °borderBevelRaised()Same # Set border style: Bevel raised
	 * °borderLoweredBevel ^ borderBevelLowered
	 * °borderBevelLowered()Same # Set border style: Bevel lowered
	 */
	private void borderSimple( final CallRuntime cr, final SIMPLE s ) {
		cr.argsNone();

		switch( s ) {
			case ETCHED_RAISED:
				this.border = BorderFactory.createEtchedBorder( EtchedBorder.RAISED );
				break;
			case ETCHED_LOWERED:
				this.border = BorderFactory.createEtchedBorder( EtchedBorder.LOWERED );
				break;
			case BEVEL_RAISED:
				this.border = BorderFactory.createRaisedBevelBorder();
				break;
			case BEVEL_LOWERED:
				this.border = BorderFactory.createLoweredBevelBorder();
				break;
		}

		this.iSet();
	}

	private void iSet() {
		if( this.border == null )
			this.border = BorderFactory.createEmptyBorder();
		Border toSet = this.border;

		if( this.title != null && this.title.length() != 0 ) {
			int pos = TitledBorder.DEFAULT_JUSTIFICATION;

			if( this.titlePos != null ) {
				if( this.titlePos == POSITION.LEFT )
					pos = TitledBorder.LEFT;
				if( this.titlePos == POSITION.RIGHT )
					pos = TitledBorder.RIGHT;
				if( this.titlePos == POSITION.CENTER )
					pos = TitledBorder.CENTER;
			}
//			TitledBorder.LEADING
//			TitledBorder.TRAILING

			toSet = BorderFactory.createTitledBorder( this.border, this.title, pos, TitledBorder.DEFAULT_POSITION );
		}

		this.panel.setBorder( toSet );
	}

	/**
	 * °setTitle(Atomic title)Same # Set the title of this panel.
	 * °setTitle(Atomic title, MagicPosition pos)Same # Set title and position of this panel.
	 */
	private void setTitle( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );

		final I_Object arg1 = cr.argType( args[0], I_Atomic.class );
		this.title = arg1 == Nil.NIL ? null : Lib_Convert.toStr( cr, arg1 ).rawString();

		if( args.length == 2 ) {
			final MagicPosition pos = cr.argType( args[1], MagicPosition.class );
			this.titlePos = pos.get();
		}

		this.iSet();
	}

}
