/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import java.awt.Component;
import java.awt.Container;

import javax.swing.BoxLayout;
import javax.swing.JComponent;

import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 2022-03-29
 * @implNote
 *           https://docs.oracle.com/javase/tutorial/uiswing/layout/box.html
 */
public class JMo_Swing_LayoutRow extends A_Swing_Object implements I_Swing_Object {

	private final Container     container = new Container();
	private final ArgCallBuffer argVertical;
	private boolean             vertical  = false;


	public JMo_Swing_LayoutRow( final Call vertical ) {
		this.argVertical = new ArgCallBuffer( 0, vertical );
	}

	@Override
	public I_Object callMethod1( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				return this.mAdd( cr );
			case "remove":
				return this.mRemove( cr );
			case "removeAll":
				return this.mRemoveAll( cr );

			default:
				return null;
		}
	}

	public Component getSwing() {
		return this.container;
	}

	@Override
	public void init1( final CallRuntime cr ) {
		this.vertical = this.argVertical.init( cr, this, JMo_Bool.class ).rawBoolean();
		this.container.setLayout( new BoxLayout( this.container, this.vertical ? BoxLayout.Y_AXIS : BoxLayout.X_AXIS ) );
	}

	@Override
	public boolean validateEvent( final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °add(Swing_Object objs...)Same # Add one or more components.
	 */
	private JMo_Swing_LayoutRow mAdd( final CallRuntime cr ) {
		final I_Swing_Object[] args = (I_Swing_Object[])cr.argsVar( this, 0, I_Swing_Object.class );

		for( final I_Swing_Object swingObj : args ) {
			final Component comp = swingObj.getSwing();

			if( this.vertical && comp instanceof JComponent )
				((JComponent)comp).setAlignmentX( Component.CENTER_ALIGNMENT );

			this.container.add( comp );
		}
		this.container.doLayout();
		this.container.repaint();

		return this;
	}

	/**
	 * °remove(Swing_Object objs...)Same # Remove one or more components.
	 */
	private JMo_Swing_LayoutRow mRemove( final CallRuntime cr ) {
		final I_Swing_Object[] args = (I_Swing_Object[])cr.argsVar( this, 0, I_Swing_Object.class );

		for( final I_Swing_Object swingObj : args )
			this.container.remove( swingObj.getSwing() );

		this.container.doLayout();
		this.container.repaint();

		return this;
	}

	/**
	 * °removeAll()Same # Remove all components.
	 */
	private JMo_Swing_LayoutRow mRemoveAll( final CallRuntime cr ) {
		cr.argsNone();
		this.container.removeAll();
		return this;
	}

}
