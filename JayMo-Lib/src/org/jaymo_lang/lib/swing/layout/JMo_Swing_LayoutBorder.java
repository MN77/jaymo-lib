/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.layout;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.lib.swing.I_Swing_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.POSITION;


/**
 * @author Michael Nitsche
 */
public class JMo_Swing_LayoutBorder extends A_Swing_Object implements I_Swing_Object {

//	private final Call[] k;
	private final Container container = new Container();


//	public SwingGroupBorder(Call... k) {
//		this.k=k;
//	}


	// ERZEUGEN


	@Override
	public I_Object callMethod1( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "put":
				return this.put( cr );

			default:
				return null;
		}
	}

	public Component getSwing() {
		return this.container;
	}

	@Override
	public void init1( final CallRuntime cr ) {
//		this.container.setLayout(new FlowLayout());
		this.container.setLayout( new BorderLayout() );
//		this.container.setBackground(Color.GREEN);
//		this.container.setSize(100, 200);
//		this.container.setPreferredSize(new Dimension(200,300));
//		for(Call call : this.k) {
//			S_Object o = call.exec(this);
//			if(!(o instanceof S_SwingObj))
//				throw new ErrorExec("This is not a Swing-Object", Lib_Konvert.typeName(o.getType()), c);
//			S_SwingObj b=(S_SwingObj)o;
//			b.callInit(c);
//			this.container.add(b.gSwing());
////			c.layout();	// this.swing()
//		}
	}

	@Override
	public boolean validateEvent( final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °put(MagicPosition pos, Swing_Object obj)Same # Put a component to a position.
	 */
	private JMo_Swing_LayoutBorder put( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, MagicPosition.class, I_Swing_Object.class );
		final MagicPosition mpos = (MagicPosition)args[0];
		final POSITION pos = mpos.get();
		final I_Swing_Object so = (I_Swing_Object)args[1];
		String dir = null;

		if( pos == POSITION.TOP )
			dir = BorderLayout.NORTH;
		if( pos == POSITION.LEFT )
			dir = BorderLayout.WEST;
		if( pos == POSITION.CENTER )
			dir = BorderLayout.CENTER;
		if( pos == POSITION.RIGHT )
			dir = BorderLayout.EAST;
		if( pos == POSITION.BOTTOM )
			dir = BorderLayout.SOUTH;
		if( dir == null )
			throw new RuntimeError( cr, "Invalid position", "Allowed are: top,left,center,right,bottom; Got: " + pos );

		this.container.add( so.getSwing(), dir );
		return this;
	}

}
