/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.jaymo_lang.lib.gui.control.A_JG_TextAreaI;
import org.jaymo_lang.lib.gui.control.A_JG_TextAreaM;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Char;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_Swing_TextArea extends A_Swing_JComponent {

	private JTextArea   item;
	private JScrollPane pane;

	private final A_JG_TextAreaM methods = new A_JG_TextAreaM() {

		@Override
		protected void setText( String text ) {
			JMo_Swing_TextArea.this.item.setText( text );
		}

		@Override
		protected String getText() {
			return JMo_Swing_TextArea.this.item.getText();
		}

		@Override
		protected void append( String text ) {
			JMo_Swing_TextArea.this.item.append( text );
		}

		@Override
		protected void setWrapLines( boolean enabled ) {
			JMo_Swing_TextArea.this.item.setLineWrap( enabled );
			JMo_Swing_TextArea.this.item.setWrapStyleWord( enabled );
		}
	};

	private final A_JG_TextAreaI inits = new A_JG_TextAreaI() {
		@Override
		protected void atKeys( CallRuntime cr, String evChange, String evExec ) {
			// Events //
			JMo_Swing_TextArea.this.item.addKeyListener( new KeyListener() {

				public void keyPressed( final KeyEvent e ) {}

				public void keyReleased( final KeyEvent e ) {

					if( e.getKeyCode() == 10 && e.getModifiersEx() == 0 ) {
						JMo_Swing_TextArea.this.eventRun( cr, evExec, JMo_Char.createSimple( e.getKeyChar() ) );
						return;
					}

					if( e.getKeyChar() != KeyEvent.CHAR_UNDEFINED )
						JMo_Swing_TextArea.this.eventRun( cr, evChange, JMo_Char.createSimple( e.getKeyChar() ) );
				}

				public void keyTyped( final KeyEvent e ) {}

			} );

		}
	};


	public JMo_Swing_TextArea() {}

	/**
	 * °set ^ setText
	 * °setText(Str text)Same # Set the text of the component.
	 * °get ^ getText
	 * °getText()Str # Get the text of this component.
	 * °append(Str text)Same # Append this text to the existing text.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JScrollPane getSwing() {
		return this.pane;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.item = new JTextArea();
		this.pane = new JScrollPane( this.item );
		this.inits.init(cr, this);
	}

	/**
	 * °@change # Executed on every change.
	 * °@exec # Executed if the Enter-Key is pressed.
	 */
	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
