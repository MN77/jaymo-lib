/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.MouseInfo;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;

import org.jaymo_lang.lib.font.JMo_Font;
import org.jaymo_lang.lib.graphic.A_ColorModel;
import org.jaymo_lang.lib.gui.control.A_JG_A_ControlI;
import org.jaymo_lang.lib.gui.control.A_JG_A_ControlM;
import org.jaymo_lang.lib.swing.A_Swing_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 11.01.2021
 */
public abstract class A_Swing_JComponent extends A_Swing_Object {

	/**
	 * °setEnabled(Bool b)Same # Enable or disable this component.
	 * °setFont(Swing_Font font)Same # Set the font of the components text.
	 * °setTip ^ setToolTip
	 * °setToolTip(Str text)Same # Set the tool tip of the image.
	 * °setVisible(Bool b)Same # Set this component visible or hide it.
	 * °setBackground(ColorSpace color)Same # Set the background color. This turns the component to opaque.
	 * °setForeground(ColorSpace color)Same # Set the foreground color.
	 */
	private final A_JG_A_ControlM methods = new A_JG_A_ControlM() {

		@Override
		protected void border() {
			Border border = BorderFactory.createLineBorder(Color.black); // TODO
			A_Swing_JComponent.this.getJComponent().setBorder( border );
		}

		@Override
		protected void focus() {
			A_Swing_JComponent.this.getSwing().requestFocusInWindow();
		}

		@Override
		protected int[] getBackground() {
			final Color backColor = A_Swing_JComponent.this.getSwing().getBackground();
			final int[] backRGB = { backColor.getRed(), backColor.getGreen(), backColor.getBlue() };
			return backRGB;
		}

		@Override
		protected int[] getForeground() {
			final Color foreColor = A_Swing_JComponent.this.getSwing().getForeground();
			final int[] foreRGB = { foreColor.getRed(), foreColor.getGreen(), foreColor.getBlue() };
			return foreRGB;
		}

		@Override
		protected boolean hasFocus() { // TODO isFocusOwner, isFocused?
			return A_Swing_JComponent.this.getSwing().isFocusOwner();
		}

		@Override
		protected boolean isEnabled() {
			return A_Swing_JComponent.this.getSwing().isEnabled();
		}

		@Override
		protected boolean isMouseOver() {
			final java.awt.Point p = MouseInfo.getPointerInfo().getLocation();
			return A_Swing_JComponent.this.getSwing().getBounds().contains( p ); // TODO Test
		}

		@Override
		protected boolean isVisible() {
			return A_Swing_JComponent.this.getSwing().isVisible();
		}

		@Override
		protected void setEnabled( boolean b ) {
			A_Swing_JComponent.this.getSwing().setEnabled( b );
		}

		@Override
		protected void setFont( CallRuntime cr, JMo_Font font ) {
			A_Swing_JComponent.this.getSwing().setFont( font.toAwtFont() );
		}

		@Override
		protected void setToolTip( CallRuntime cr, String toolTip ) {
			A_Swing_JComponent.this.getJComponent().setToolTipText( toolTip );
		}

		@Override
		protected void setVisible( boolean b ) {
			A_Swing_JComponent.this.getSwing().setVisible( b );
		}

		@Override
		protected void setBackgroundNone( CallRuntime cr ) {
			A_Swing_JComponent.this.getSwing().setBackground( null );
		}

		@Override
		protected void setBackgroundColor( CallRuntime cr, A_ColorModel color ) {
//			final Component comp = this.getSwing();
//			if( color != null && comp instanceof JComponent )
			A_Swing_JComponent.this.getJComponent().setOpaque( true ); // Not transparent
			A_Swing_JComponent.this.getSwing().setBackground( color.getColor() );
		}

		@Override
		protected void setBackgroundRGB( CallRuntime cr, int[] rgb ) {
			final Color color = new Color(rgb[0], rgb[1], rgb[2]);
			A_Swing_JComponent.this.getJComponent().setOpaque( true ); // Not transparent
			A_Swing_JComponent.this.getSwing().setBackground( color );
		}

		@Override
		protected void setForegroundNone( CallRuntime cr ) {
			A_Swing_JComponent.this.getSwing().setForeground( null );
		}

		@Override
		protected void setForegroundColor( CallRuntime cr, A_ColorModel color ) {
			A_Swing_JComponent.this.getJComponent().setOpaque( true ); // Not transparent
			A_Swing_JComponent.this.getSwing().setForeground( color.getColor() );
		}

		@Override
		protected void setForegroundRGB( CallRuntime cr, int[] rgb ) {
			final Color color = new Color(rgb[0], rgb[1], rgb[2]);
			A_Swing_JComponent.this.getJComponent().setOpaque( true ); // Not transparent
			A_Swing_JComponent.this.getSwing().setForeground( color );
		}
	};

	private final A_JG_A_ControlI inits = new A_JG_A_ControlI() {

		@Override
		protected void atMouseClick( CallRuntime cr, String event ) {}

		@Override
		protected void atMouseUp( CallRuntime cr, String event ) {}

		@Override
		protected void atMouseDoubleClick( CallRuntime cr, String event ) {}

		@Override
		protected void atKey( CallRuntime cr, String event ) {}

		@Override
		protected void atKeyUp( CallRuntime cr, String event ) {}

		@Override
		protected void atMove( CallRuntime cr, String event ) {}

		@Override
		protected void atResize( CallRuntime cr, String event ) {}
	};

	@Override
	public final void init1(CallRuntime cr) {
		this.inits.init( cr, this );
		this.init2(cr);
	}

	protected abstract void init2( CallRuntime cr );

	@Override
	public I_Object callMethod1( final CallRuntime cr, final String method ) {
		final I_Object result = this.methods.call( cr, method, this );
		if(result !=null)
			return result;

		switch(method) {
			case "setTransparent":
				this.mSetOpaque( cr, true );
				return this;
			case "setOpaque":
				this.mSetOpaque( cr, false );
				return this;
			case "setSize":
				this.mSetSize( cr );
				return this;

			default:
				return this.callMethod2( cr, method );
		}
	}

	protected abstract I_Object callMethod2( CallRuntime cr, String method );

	@Override
	public final boolean validateEvent( final String event ) {
		return this.inits.validateEvent( event )
			? true
			: this.validateEvent2(event);
	}

	protected abstract boolean validateEvent2( String event );

	protected final JComponent getJComponent() {
		return (JComponent)this.getSwing();
	}

	/**
	 * °setTransparent(Bool b)Same # Set this component transparent.
	 * °setOpaque(Bool b)Same # Set this component opaque.
	 */
	private void mSetOpaque( final CallRuntime cr, final boolean invert ) {
		final JMo_Bool arg = (JMo_Bool)cr.args( this, JMo_Bool.class )[0];
		boolean b = arg.rawBoolean();

		if( invert )
			b = !b;

		this.getJComponent().setOpaque( b );
	}

	/**
	 * °setSize(Int width, Int height)Same # Set the preferred size of this component.
	 */
	private void mSetSize( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Int.class, JMo_Int.class );
		final int w = Lib_Convert.toInt( cr, args[0] );
		final int h = Lib_Convert.toInt( cr, args[1] );
//		((JComponent)this.getSwing()).setSize(w, h);
		this.getSwing().setPreferredSize( new Dimension( w, h ) );
	}

}
