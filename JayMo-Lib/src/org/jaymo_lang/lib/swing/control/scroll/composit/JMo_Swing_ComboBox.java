/*******************************************************************************
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit;

import javax.swing.JComboBox;

import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_ComboBoxI;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_ComboBoxM;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 02.04.2022
 */
public class JMo_Swing_ComboBox extends A_Swing_JComponent {

	private JComboBox<String> combo;
	private final ArgCallBuffer arg0readOnly;

	private final A_JG_ComboBoxM methods = new A_JG_ComboBoxM() {
		@Override
		public String getValue() {
			return (String)JMo_Swing_ComboBox.this.combo.getSelectedItem();
		}

		@Override
		public void setItems( String[] items ) {
			JMo_Swing_ComboBox.this.combo.removeAllItems();
			this.add(items);
		}

		@Override
		public void clear() {
			JMo_Swing_ComboBox.this.combo.removeAll();
		}

		@Override
		public void setValue( String s ) {
			JMo_Swing_ComboBox.this.combo.setSelectedItem( s );
		}

		@Override
		public void add( String[] items ) {
			for( final String item : items )
				JMo_Swing_ComboBox.this.combo.addItem( item );
		}
	};

	private final A_JG_ComboBoxI inits = new A_JG_ComboBoxI() {
		@Override
		protected void arg0readOnly( boolean readOnly ) {
			JMo_Swing_ComboBox.this.combo.setEditable( !readOnly );
		}

		@Override
		protected void atChange( CallRuntime cr, String event ) {
			JMo_Swing_ComboBox.this.combo.addPropertyChangeListener( e -> JMo_Swing_ComboBox.this.eventRun( cr, event, JMo_Swing_ComboBox.this ) );
		}
	};


	/**
	 * +Swing_ComboBox()
	 * +Swing_ComboBox(boolean readOnly)
	 */
	public JMo_Swing_ComboBox() {
		this.arg0readOnly = null;
	}

	public JMo_Swing_ComboBox( final Call readOnly ) {
		this.arg0readOnly = new ArgCallBuffer( 0, readOnly );
	}

	/**
	 * °get ^ getItems
	 * °getItem()Str # Returns the selected item.
	 * °set ^ setItems
	 * °setItems()Str # Set the selectable options.
	 */
	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JComboBox<String> getSwing() {
		return this.combo;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.combo = new JComboBox<>();
		this.inits.init( cr, this, this.arg0readOnly );
//		this.button.addActionListener(e -> JMo_Swing_ComboBox.this.eventRun(cr, "@select", this));
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		final int amount = this.combo == null ? 0 : this.combo.getItemCount();
		return this.toString() + "<" + amount + ">";
	}

	/**
	 * #°@select # Executed on selection.
	 */
	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}

}
