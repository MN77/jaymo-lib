/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Java;


/**
 * @author Michael Nitsche
 * @created 04.04.2020
 */
public class JMo_Swing_Frame extends A_Swing_Window {

	private JFrame frame = null;


	public JMo_Swing_Frame( final Call parent ) {
		super( parent );
	}

	public JMo_Swing_Frame( final Call parent, final Call title ) {
		super( parent, title );
	}

	public JMo_Swing_Frame( final Call parent, final Call dx, final Call dy ) {
		super( parent, dx, dy );
	}

	/**
	 * ! Object of a single Swing-Frame (sub window)
	 * + Swing_Frame(Swing_Window parent)
	 * + Swing_Frame(Swing_Window parent, Str title)
	 * + Swing_Frame(Swing_Window parent, IntNumber dx, IntNumber dy)
	 * + Swing_Frame(Swing_Window parent, IntNumber dx, IntNumber dy, Str title)
	 */
	public JMo_Swing_Frame( final Call parent, final Call dx, final Call dy, final Call title ) {
		super( parent, dx, dy, title );
	}

	public Component getSwing() {
		return this.frame;
	}

	@Override
	public void init2( final CallRuntime cr ) {}

	@Override
	public boolean validateEvent( final String event ) {
		return false;
	}

	@Override
	protected I_Object callEvent( final CallRuntime cr, final String event ) {
		return null;
	}

	/**
	 * °show()Same # Show this window.
	 * °hide()Same # Hide this window.
	 * °close()Nil # Closes this window.
	 */
	@Override
	protected I_Object callMethod2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "close": // TODO Alias dispose
				cr.argsNone();
				this.closeFrame( cr, true );
				return Nil.NIL;

			case "show":
				this.mShow( cr );
				return this;
			case "hide":
				cr.argsNone();
				SwingUtilities.invokeLater( () -> {
					this.frame.setVisible( false );
				} );
				return this;

			default:
				final I_Object res = Lib_Java.mExec( cr, this.frame, this.frame.getClass(), method, this ); //cr.parsFlex(null, this, 0)	//TODO prüfen!!!
				if( res != null && res == Nil.NIL )
					return this;

				return null;
		}
	}

	@Override
	protected Window pCreateFrame( final Frame parent ) {
		return this.frame = new JFrame();
	}

	@Override
	protected String pGetTitle() {
		return this.frame.getTitle();
	}

	@Override
	protected void pReplace( final Component swing ) {
		this.frame.add( swing );
	}

	@Override
	protected void pSetTitle( final String title ) {
		this.frame.setTitle( title );
	}

	private void mShow( final CallRuntime cr ) {
		cr.argsNone();

		if( !this.isRunning() )
			this.frame.setLocationRelativeTo( null ); // Center window
		this.setRunState( cr, false );

		SwingUtilities.invokeLater( () -> {
			this.frame.setVisible( true );
		} );
	}

}
