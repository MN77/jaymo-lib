/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll.composit;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.jaymo_lang.lib.gui.JG_Events;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_TableI;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_TableM;
import org.jaymo_lang.lib.swing.control.A_Swing_JComponent;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;


/**
 * @author Michael Nitsche
 * @created 24.11.2020
 */
public class JMo_Swing_Table extends A_Swing_JComponent {

	private JTable        table;
	private JScrollPane   pane;
	private ArgCallBuffer arg0 = null; // Init-Table
	private JMo_Table     data      = null;

	/**
	 * °set ^ setData
	 * °setData(Table tab)Same # Set the data of the table.
	 * °selectionRow()List # Returns the first selected row as a List.
	 * °selectionTable()Table # Returns the selection as table.
	 * °selectAll()Same # Select all.
	 * °selectNone()Same # Remove selections.
	 * °scrollTop()Same # Scroll up to the top.
	 * °selectRow(IntNumber pos)Same # Select a single row.
	 * °selectRows(List positions)Same # Select some rows.
	 * °selectRows(IntNumber from, Int to)Same # Select all lines from 'from' to 'to'.
	 */
	private final A_JG_TableM methods = new A_JG_TableM() {

		@Override
		public void setTable( CallRuntime cr, JMo_Table table ) {
			JMo_Swing_Table.this.iSetData( cr, table );
		}

		@Override
		public I_Object getSelectionRow() {
			final int selected = JMo_Swing_Table.this.table.getSelectedRow();
			if( selected == -1 || JMo_Swing_Table.this.data == null )
				return Nil.NIL;
			final I_Object[] row1 = JMo_Swing_Table.this.data.getInternalObject().getRow( selected );
			final SimpleList<I_Object> al = new SimpleList<>( row1.length );
			al.addMore( row1 );
			return new JMo_List( al );
		}

		@Override
		public JMo_Table getSelectionTable() {
			final int[] selected = JMo_Swing_Table.this.table.getSelectedRows();
			final I_Table<I_Object> source = JMo_Swing_Table.this.data.getInternalObject();
			final ArrayTable<I_Object> result = new ArrayTable<>( source.width() );

			for( final int sel : selected ) {
				final I_Object[] row = source.getRow( sel );
				result.add( row );
			}
			return new JMo_Table( result );
		}

		@Override
		public void selectRows( final CallRuntime cr, int[] ia ) {
			JMo_Swing_Table.this.table.clearSelection();

			for( final int index : ia )
				JMo_Swing_Table.this.table.addRowSelectionInterval( index, index );

			JMo_Swing_Table.this.eventRun( cr, JG_Events.SELECT, JMo_Swing_Table.this );
		}

		@Override
		public void selectRow( final CallRuntime cr, int index ) {
			JMo_Swing_Table.this.table.clearSelection();
			JMo_Swing_Table.this.table.addRowSelectionInterval( index, index );

			JMo_Swing_Table.this.eventRun( cr, JG_Events.SELECT, JMo_Swing_Table.this );
		}

		@Override
		public void selectRange( final CallRuntime cr, int from, int to ) {
			JMo_Swing_Table.this.table.clearSelection();
			JMo_Swing_Table.this.table.addRowSelectionInterval( from, to );
			JMo_Swing_Table.this.eventRun( cr, JG_Events.SELECT, JMo_Swing_Table.this );
		}

		@Override
		public void scrollTop() {
			javax.swing.SwingUtilities.invokeLater( () -> JMo_Swing_Table.this.pane.getVerticalScrollBar().setValue( 0 ) );
		}

		@Override
		public void selectAll(CallRuntime cr) {
			JMo_Swing_Table.this.table.selectAll();
			JMo_Swing_Table.this.eventRun( cr, "@select", JMo_Swing_Table.this );
		}

		@Override
		public void selectNone() {
			JMo_Swing_Table.this.table.clearSelection();
		}
	};

	private final A_JG_TableI inits = new A_JG_TableI() {

		@Override
		protected void arg0table( CallRuntime cr, JMo_Table table ) {
			JMo_Swing_Table.this.iSetData( cr, table );
		}

		@Override
		protected void atSelect(CallRuntime cr, String ev) {
			JMo_Swing_Table.this.table.addMouseListener( new MouseListener() {

				public void mouseClicked( final MouseEvent e ) {
					JMo_Swing_Table.this.eventRun( cr, ev, JMo_Swing_Table.this ); // TODO Bei Tastatur ...
				}

				public void mouseEntered( final MouseEvent e ) {}

				public void mouseExited( final MouseEvent e ) {}

				public void mousePressed( final MouseEvent e ) {}

				public void mouseReleased( final MouseEvent e ) {}

			} );
		}
	};


	public JMo_Swing_Table() {}

	public JMo_Swing_Table( final Call table ) {
		this.arg0 = new ArgCallBuffer( 0, table );
	}

	@Override
	public I_Object callMethod2( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );

/*
 * getSelection
 * setSelection
 * getSelectionRow
 * setSelectionRow
 * ... ?!?
 */
	}

	public JComponent getSwing() {
		return this.pane;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.table = new JTable();
		this.pane = new JScrollPane( this.table );

		this.inits.init( cr, this, this.arg0 );
	}

	/**
	 * °@select # Executed on selection.
	 */
	@Override
	protected boolean validateEvent2( String event ) {
		return this.inits.validateEvent( event );
	}


	private void iSetData( final CallRuntime cr, final JMo_Table table ) {
		final I_Table<I_Object> al = table.getInternalObject();

		final AbstractTableModel model = new AbstractTableModel() {

			private static final long serialVersionUID = 1L;


			public int getColumnCount() {
				return al.width();
			}

			@Override
			public String getColumnName( final int column ) {
				final I_Object[] names = table.getInternalColumnNames();
				return names == null ? "" : names[column].toString();
			}

			public int getRowCount() {
				return al.size();
			}

			public Object getValueAt( final int row, final int col ) {
				return al.get( col, row ); // toString?
			}

			@Override
			public boolean isCellEditable( final int row, final int column ) {
				return false; // true
			}

//          @Override
//			public void setValueAt(Object value, int row, int col) {
//              rowData[row][col] = value;
//              this.fireTableCellUpdated(row, col);
//          }
		};

		this.table.setModel( model );
	}

	/**
	 * °selection()List # Returns a List with all selected line numbers.
	 */
//	private I_Object mSelection( final CallRuntime cr ) {
//		cr.argsNone();
//		final int[] selected = this.table.getSelectedRows();
//		final SimpleList<I_Object> al = new SimpleList<>( selected.length );
//		for( final int element : selected )
//			al.add( new JMo_Int( element + 1 ) );
//		return new JMo_List( al );
//	}


//	@Override
//	protected String resolveDefault() {
//		return "@select";
//	}

}
