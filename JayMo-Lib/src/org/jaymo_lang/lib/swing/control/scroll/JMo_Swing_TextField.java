/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.control.scroll;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.jaymo_lang.lib.gui.control.scroll.A_JG_TextFieldI;
import org.jaymo_lang.lib.gui.control.scroll.A_JG_TextFieldM;
import org.jaymo_lang.lib.swing.control.A_Swing_JTextComponent;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Char;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.POSITION_H;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_Swing_TextField extends A_Swing_JTextComponent {

	private final ArgCallBuffer arg0;
	private JTextField textField;

	/**
	 * °setAlign(MagicPosition pos)Same # Set alignment of the text.
	 */
	private final A_JG_TextFieldM methods = new A_JG_TextFieldM() {
		@Override
		protected void setAlign(POSITION_H align) {
			final int alignment = align == POSITION_H.LEFT ? SwingConstants.LEFT : align == POSITION_H.CENTER ? SwingConstants.CENTER : SwingConstants.RIGHT;
			JMo_Swing_TextField.this.textField.setHorizontalAlignment( alignment );
		}

		@Override
		protected void setEditable( boolean b ) {
			JMo_Swing_TextField.this.textField.setEditable( b );
		}

		@Override
		protected void setText( String s ) {
			JMo_Swing_TextField.this.textField.setText( s );
		}

		@Override
		protected String getText() {
			return JMo_Swing_TextField.this.textField.getText();
		}
	};

	private final A_JG_TextFieldI inits = new A_JG_TextFieldI() {

		@Override
		protected void arg0str( JMo_Str s ) {
			JMo_Swing_TextField.this.textField.setText( s.rawString() );
		}

		@Override
		protected void atChange( CallRuntime cr, String event ) {
			JMo_Swing_TextField.this.textField.addActionListener( e -> JMo_Swing_TextField.this.eventRun( cr, event, JMo_Swing_TextField.this ) );
		}

		@Override
		protected void atExec( CallRuntime cr, String event ) {
			JMo_Swing_TextField.this.textField.addActionListener( e -> JMo_Swing_TextField.this.eventRun( cr, event, JMo_Swing_TextField.this ) );
		}
	};


	public JMo_Swing_TextField() {
		this.arg0 = null;
	}

	public JMo_Swing_TextField( final Call initText ) {
		this.arg0 = new ArgCallBuffer(0, initText );
	}

	@Override
	public I_Object callMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	public JTextField getSwing() {
		return this.textField;
	}

	@Override
	public void init2( final CallRuntime cr ) {
		this.textField = new JTextField();
		this.inits.init(cr, this, this.arg0);

		this.textField.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {}

			public void keyReleased( final KeyEvent e ) {

				if( e.getKeyCode() == 10 && e.getModifiersEx() == 0 ) {
					JMo_Swing_TextField.this.eventRun( cr, "@exec", JMo_Char.createSimple( e.getKeyChar() ) );
					return;
				}

				if( e.getKeyChar() != KeyEvent.CHAR_UNDEFINED )
					JMo_Swing_TextField.this.eventRun( cr, "@change", JMo_Char.createSimple( e.getKeyChar() ) );
			}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	/**
	 * °@change # Executed on every change.
	 * °@exec # Executed if the Enter-Key is pressed.
	 */
	@Override
	public boolean validateEvent2( final String event ) {
		return this.inits.validateEvent( event );
	}

//	@Override
//	protected String resolveDefault() {
//		return "@change";
//	}

}
