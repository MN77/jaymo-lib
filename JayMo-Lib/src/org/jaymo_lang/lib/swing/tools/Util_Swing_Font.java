/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.swing.tools;

import java.awt.Font;

import org.jaymo_lang.lib.font.JMo_Font;

/**
 * @author	Michael Nitsche
 * @created	14.05.2024
 */
public class Util_Swing_Font {

	private Util_Swing_Font() {}


	public static Font toSwingFont( JMo_Font f ) {
		int style = Font.PLAIN;

		if( f.isBold() )
			style = style | Font.BOLD;
		if( f.isItalic() )
			style = style | Font.ITALIC;

		return new Font( f.getName(), style, f.getSize() );
	}
}
