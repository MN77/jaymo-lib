/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.css;

import java.util.List;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_Tree;
import org.jaymo_lang.object.struct.JMo_TreeNode;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_String;


/**
 * @author Michael Nitsche
 * @created 2020-10-26
 */
public class JMo_Css extends A_ObjectSimple {

	final ArgCallBuffer tree;


	/**
	 * ! Object of a Cascading Style Sheet Tree
	 * + Css(Tree tree)
	 */
	public JMo_Css( final Call tree ) {
		this.tree = new ArgCallBuffer( 0, tree );
	}

	@Override
	public void init( final CallRuntime cr ) {
//		if(this.buffer_tree != null)
		this.tree.init( cr, this, JMo_Tree.class );
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "export":
				return this.export( cr );

			default:
				return null;
		}
	}

	/**
	 * °export()Str # Export to CSS.
	 */
	private I_Object export( final CallRuntime cr ) {
		cr.argsNone();
		final StringBuilder sb = new StringBuilder();
//		Group2<I_Atomic, I_Object> name_value = this.tree.internalNameValue();
		final List<JMo_TreeNode> nodes = ((JMo_TreeNode)this.tree.get()).internalNodes();

		for( final JMo_TreeNode node : nodes )
			this.iExportNode( sb, node, 0 );

		return new JMo_Str( sb.toString() );
	}

	private void iExportNode( final StringBuilder sb, final JMo_TreeNode node, final int indent ) {
		final Group2<I_Atomic, I_Object> name_value = node.internalNameValue();
		final List<JMo_TreeNode> nodes = node.internalNodes();

		if( nodes.size() > 0 ) {
			final String space = Lib_String.sequence( '\t', indent );

			sb.append( space + name_value.o1.toString() + " {\n" );

			for( final JMo_TreeNode subNode : nodes )
				this.iExportNode( sb, subNode, indent + 1 );

			sb.append( space + "}\n\n" );
		}
		else {
			sb.append( Lib_String.sequence( '\t', indent ) );
			sb.append( name_value.o1.toString() );
			sb.append( ": " );
			sb.append( name_value.o2.toString() );
			sb.append( ";\n" );
		}
	}

}
