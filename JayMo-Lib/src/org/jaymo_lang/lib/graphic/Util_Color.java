/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 28.11.2021
 */
public class Util_Color {

	private Util_Color() {}

	
	/**
	 * @apiNote Accept 1 argument = Color, 3 arguments = RGB as Int
	 */
	public static int[] argsToRGB( final CallRuntime cr, final I_Object[] args ) {
		switch( args.length ) {
			case 1:
				final A_ColorModel color = cr.argType( args[0], A_ColorModel.class );
				return Util_Color.colorToRGB(color);
			case 2:
				throw new CodeError( "Invalid amount of arguments", "Allowed are 1 or 3, but got 2 for color", cr.getDebugInfo() );
			case 3:
				int r = Lib_Convert.toInt( cr, cr.argType( args[0], JMo_Int.class ) );
				int g = Lib_Convert.toInt( cr, cr.argType( args[1], JMo_Int.class ) );
				int b = Lib_Convert.toInt( cr, cr.argType( args[2], JMo_Int.class ) );
				Err.ifOutOfBounds( 0, 255, r );
				Err.ifOutOfBounds( 0, 255, g );
				Err.ifOutOfBounds( 0, 255, b );
				return new int[]{ r, g, b };
			default:
				throw new CodeError( "Invalid amount of arguments", "Allowed are 1 or 3, but got " + args.length + " for color", cr.getDebugInfo() );
		}
	}

	public static int[] colorToRGB( A_ColorModel color ) {
		int r = color.getColor().getRed();
		int g = color.getColor().getGreen();
		int b = color.getColor().getBlue();
		return new int[]{ r, g, b };
	}

	public static int diffValue( final int v1, final int v2 ) {
		return Math.max( v1, v2 ) - Math.min( v1, v2 );
	}

}
