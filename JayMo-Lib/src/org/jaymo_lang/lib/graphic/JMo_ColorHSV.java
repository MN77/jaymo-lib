/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.graphic;

import java.awt.Color;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.util.Lib_Math;
import de.mn77.lib.graphic.Lib_Graphic;


/**
 * @author Michael Nitsche
 * @created 27.02.2021
 */
public class JMo_ColorHSV extends A_ColorModel {

	private final ArgCallBuffer par1, par2, par3;
	private int                 hue        = -1;
	private double              saturation = -1, value = -1;


	/**
	 * + ColorHSV()
	 */
	public JMo_ColorHSV() {
		this( Color.black );
	}

	/**
	 * + ColorHSV(IntNumber hue, Number saturation, IntNumber value)
	 */
	public JMo_ColorHSV( final Call p1, final Call p2, final Call p3 ) {
		this.par1 = new ArgCallBuffer( 0, p1 );
		this.par2 = new ArgCallBuffer( 1, p2 );
		this.par3 = new ArgCallBuffer( 2, p3 );
	}

	public JMo_ColorHSV( final Color c ) {
		this.par1 = null;
		this.par2 = null;
		this.par3 = null;
		final float[] hsv = Lib_Graphic.RGBtoHSV( c.getRed(), c.getGreen(), c.getBlue() );
		this.hue = Math.round( hsv[0] );
		this.saturation = hsv[1];
		this.value = hsv[2];
	}

	@Override
	public Color getColor() {
		final int[] rgb = Lib_Graphic.HSVtoRGB( this.hue, (float)this.saturation, (float)this.value );
		return new Color( rgb[0], rgb[1], rgb[2] );
	}

	@Override
	public void init( final CallRuntime cr ) {

		if( this.par1 != null && this.par2 != null && this.par3 != null ) {
			final I_IntNumber po1 = this.par1.init( cr, this, I_IntNumber.class );
			final I_Number po2 = this.par2.init( cr, this, I_Number.class );
			final I_Number po3 = this.par3.init( cr, this, I_Number.class );
			final double pi1 = Lib_Convert.toDouble( cr, po1 );
			final double pi2 = Lib_Convert.toDouble( cr, po2 );
			final double pi3 = Lib_Convert.toDouble( cr, po3 );
			Lib_Error.ifNotBetween( cr, 0, 360, pi1, "Value for hue" );
			Lib_Error.ifNotBetween( cr, 0, 1, pi2, "Value for saturation" );
			Lib_Error.ifNotBetween( cr, 0, 1, pi3, "Value for brightness" );
			this.hue = Lib_Math.roundToInt( pi1 );
			this.saturation = pi2;
			this.value = pi3;
		}
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {

		switch( type ) {
			case NESTED:
				return Lib_Type.getName( this );
			case DESCRIBE:
				return Lib_Type.getName( this ) + "(" + this.hue + "i," + Lib_Math.round( this.saturation, 5 ) + "d," + Lib_Math.round( this.value, 5 ) + "d)";
			default:
				return Lib_Type.getName( this ) + "(" + this.hue + "," + Lib_Math.round( this.saturation, 5 ) + "," + Lib_Math.round( this.value, 5 ) + ")";
		}
	}

	/**
	 * °hue()Int # Returns the hue.
	 * °saturation()Dec # Returns the saturation as decimal number from 0 to 1.
	 * °value ^ brightness
	 * °brightness()Dec # Returns the brightness/value as decimal number from 0 to 1.
	 */
	@Override
	protected I_Object call3( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "hue":
				cr.argsNone();
				return new JMo_Int( this.hue );
			case "saturation":
				cr.argsNone();
				return JMo_Dec.valueOf( cr, this.saturation );
			case "value":
			case "brightness":
				cr.argsNone();
				return JMo_Dec.valueOf( cr, this.value );

			default:
				return null;
		}
	}

}
