/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.miniconf;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err_FileSys;
import de.mn77.miniconf.MIniConf;
import de.mn77.miniconf.result.MIniConfResult;


/**
 * @author Michael Nitsche
 * @created 11.02.2022
 */
public class JMo_MIniConf extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "parse":
				return this.mParse( cr );
			case "read":
				return this.mRead( cr );
		}
		return null;
	}

	/**
	 * °parse(Str s)MIniConfResult # Parse the given string and return a MIniConfResult
	 */
	private I_Object mParse( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		final String s = Lib_Convert.toStr( cr, arg ).rawString();
		final MIniConfResult result = MIniConf.parse( s );
		return new JMo_MIniConfResult( result );
	}

	/**
	 * °read(File f)MIniConfResult # Parse the given file and return a MIniConfResult
	 * °read(Str file)MIniConfResult # Parse the given file and return a MIniConfResult
	 */
	private I_Object mRead( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_File.class );

		if( arg instanceof JMo_File )
			try {
				final MIniConfResult result = MIniConf.parse( ((JMo_File)arg).getInternalFile() );
				return new JMo_MIniConfResult( result );
			}
			catch( final Err_FileSys e ) {
				throw new ExternalError( cr, "File read error", e.getMessage() );
			}
		else {
			final String s = Lib_Convert.toStr( cr, arg ).rawString();
			final MIniConfResult result = MIniConf.parse( s );
			return new JMo_MIniConfResult( result );
		}
	}

}
