/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.runtime.CallRuntime;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import de.mn77.base.stream.UTF8InputStream;


/**
 * @author Michael Nitsche
 * @created 12.03.2021
 *
 *          TODO XmlReader, XmlParser, XmlGenerator?
 */
public class JMo_Xml extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "read":
			case "readFile":
				return this.readFile( cr );
			case "parse":
				return this.parse( cr );
		}
		return null;
	}

	/**
	 * °parse(Str xml)XmlDocument # Parse a xml string.
	 */
	private JMo_XmlDocument parse( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_File.class );

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			final DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = null;
			final String s = ((JMo_Str)arg).rawString();
			doc = builder.parse( new UTF8InputStream( s ) );
			return new JMo_XmlDocument( doc );
		}
		catch( ParserConfigurationException | SAXException e ) {
			throw new RuntimeError( cr, "XML parse error", e.getMessage() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "XML read error", e.getMessage() );
		}
	}

	/**
	 * °read ^ readFile
	 * °readFile(File file)XmlDocument # Read and parse a xml file.
	 */
	private JMo_XmlDocument readFile( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_File.class );

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			final DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = null;
			final File f = ((JMo_File)arg).getInternalFile();
			doc = builder.parse( f ); //TODO throws catch
			return new JMo_XmlDocument( doc );
		}
		catch( ParserConfigurationException | SAXException e ) {
			throw new RuntimeError( cr, "XML parse error", e.getMessage() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "XML read error", e.getMessage() );
		}
	}

}
