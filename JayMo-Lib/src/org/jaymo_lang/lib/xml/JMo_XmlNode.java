/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.xml;

import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public class JMo_XmlNode extends A_ObjectSimple {

	protected Node node = null;

//	public JMo_XmlNode() {}


	public JMo_XmlNode( final Node n ) {
		this.node = n;
	}

	@Override
	protected final I_Object call2( final CallRuntime cr, final String method ) {

		/**
		 * °name()Str # Returns the name of this node.
		 * °text()Str # Returns the text of this node.
		 */
		switch( method ) {
			case "name": // getName
				cr.argsNone();
				return new JMo_Str( this.node.getNodeName() );
//			case "value": // getValue
//				cr.argsNone();
//				return new Str(this.node.getNodeValue());
			case "text":
				cr.argsNone();
				return new JMo_Str( this.node.getTextContent() ); // TODO Nil ?!?
			case "nodes": // getNodes
				return this.mNodes( cr );
			case "attr": // getAttr
			case "attribute": // getAttribute
				return this.mAttribute( cr );
			case "attributes":
				return this.mAttributes( cr );

			case "stringTable":
			case "strTable":
				return this.mStrTable( cr );

			case "get":
			case "getFirst":
				return this.mGetFirst( cr );
		}

		return this.call4( cr, method );
	}

	protected I_Object call4( final CallRuntime cr, final String method ) {
		return null;
	}

	private void iNodes( final CallRuntime cr, final SimpleList<I_Object> list, final Node node, final I_Object[] args, final int argOffset ) {
		final String filter = Lib_Convert.toStr( cr, args[argOffset] ).rawString();

		if( node.getNodeName().equals( filter ) )
			if( args.length - 1 == argOffset )
				list.add( new JMo_XmlNode( node ) );
			else
				this.iNodes( cr, list, node, args, argOffset + 1 );

		if( argOffset < args.length - 1 ) {
			final NodeList nodes = node.getChildNodes();

			for( int idx = 0; idx < nodes.getLength(); idx++ ) {
				final Node n = nodes.item( idx );

//			if(n.getNodeName().equals(filter)) {
//				if(args.length-1 == argOffset)
//					list.add(new JMo_XmlNode(n));
//				else
				this.iNodes( cr, list, n, args, argOffset + 1 );
//			}
			}
		}
	}

	/**
	 * °attr ^ attribute
	 * °attribute(Str name)Str? # Returns the value of a attribute or Nil if nothing is found.
	 */
	private I_Object mAttribute( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String name = Lib_Convert.toStr( cr, arg ).rawString();
		final NamedNodeMap attr = this.node.getAttributes();
		if( attr == null )
			return Nil.NIL;

		final Node n = attr.getNamedItem( name );
		return n == null
			? Nil.NIL
			: new JMo_Str( n.getNodeValue() );
	}

	/**
	 * °attributes()Map # Returns a Map with all attributes an values.
	 */
	private I_Object mAttributes( final CallRuntime cr ) {
		cr.argsNone();
		final NamedNodeMap attr = this.node.getAttributes();

		if( attr == null )
			return new JMo_Map();

		final SimpleList<I_Object> keys = new SimpleList<>();
		final SimpleList<I_Object> values = new SimpleList<>();

		for( int i = 0; i < attr.getLength(); i++ ) {
			final Node n = attr.item( i );
			final String key = n.getNodeName();
			final String value = n.getNodeValue();

			keys.add( new JMo_Str( key ) );
			values.add( new JMo_Str( value ) );
		}

		return new JMo_Map( keys, values );
	}

	/**
	 * °get ^ getFirst
	 * °getFirst(Str name) XmlNode? # Returns the first node with this name.
	 */
	private I_Object mGetFirst( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String name = Lib_Convert.toStr( cr, arg ).rawString();
		final NodeList nodes = this.node.getChildNodes();

		for( int idx = 0; idx < nodes.getLength(); idx++ ) {
			final Node n = nodes.item( idx );
			if( n.getNodeName().equals( name ) )
				return new JMo_XmlNode( n );
		}

		return Nil.NIL;
	}

	/**
	 * °nodes()List # Create a list with all node
	 * °nodes(Str filter...)List # Create a list with all nodes matching 'filter'
	 */
	private JMo_List mNodes( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVar( this, 0, I_Object.class );

		final SimpleList<I_Object> result = new SimpleList<>();
		final NodeList nodes = this.node.getChildNodes();

		for( int idx = 0; idx < nodes.getLength(); idx++ ) {
			final Node n = nodes.item( idx );

			if( args.length == 0 )
				result.add( new JMo_XmlNode( n ) );
			else
//				this.iNodes(cr, result, this.node, args, 0);
				this.iNodes( cr, result, n, args, 0 );
		}

		return new JMo_List( result );
	}

	/**
	 * °stringTable ^ strTable
	 * °strTable(List rows, List columns)Table # Create a table.
	 */
	private JMo_Table mStrTable( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_List.class, JMo_List.class ); // Second can contain Strings and Lists with Strings!
		final JMo_List rows = (JMo_List)args[0];
		final JMo_List cols = (JMo_List)args[1];

		if( this.node == null )
			throw new JayMoError( cr, "Nothing to read", "No document found!" );

		return Util_XmlTextTable.createTextTable( cr, this.node, rows, cols );
	}

}
