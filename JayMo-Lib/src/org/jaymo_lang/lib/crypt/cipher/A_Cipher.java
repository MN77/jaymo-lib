/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt.cipher;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.lib.crypt.cipher.I_Cipher;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public abstract class A_Cipher extends A_ObjectSimple {

	private byte[] key = null;


	@Override
	protected final I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setKey":
				this.setKey( cr );
				return this;
			case "encrypt":
				final byte[] encrypted = this.encrypt( cr );
				return new JMo_ByteArray( encrypted );
			case "decrypt":
				final byte[] decrypted = this.decrypt( cr );
				return new JMo_ByteArray( decrypted );
		}

		return this.call3( cr, method );
	}

	protected abstract I_Object call3( CallRuntime cr, String method );

	protected abstract I_Cipher getCipher();

	/**
	 * °decrypt(ByteArray data)ByteArray # Decrypt a ByteArray.
	 */
	private byte[] decrypt( final CallRuntime cr ) {
		final JMo_ByteArray dataObj = (JMo_ByteArray)cr.args( this, JMo_ByteArray.class )[0];
		return this.getCipher().decrypt( dataObj.getValue(), this.key );
	}

	/**
	 * °encrypt(ByteArray data)ByteArray # Encrypt a ByteArray.
	 */
	private byte[] encrypt( final CallRuntime cr ) {
		final JMo_ByteArray dataObj = (JMo_ByteArray)cr.args( this, JMo_ByteArray.class )[0];
		return this.getCipher().encrypt( dataObj.getValue(), this.key );
	}

	/**
	 * °setKey(ByteArray bytes)Same # Set the key code as a ByteArray.
	 */
	private void setKey( final CallRuntime cr ) {
		final JMo_ByteArray keyObj = (JMo_ByteArray)cr.args( this, JMo_ByteArray.class )[0];
		final byte[] keyBA = keyObj.getValue();
		Lib_Error.ifNotBetween( cr, this.getCipher().keyLengthMin(), this.getCipher().keyLengthMax(), keyBA.length, "key length" );
		this.key = keyBA;
	}

}
