/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt.cipher;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.lib.crypt.cipher.JAVA_CIPHER;
import de.mn77.lib.crypt.cipher.JavaCipher;


/**
 * @author Michael Nitsche
 * @created 14.03.2021
 */
public class JMo_CipherBlowfish extends A_Cipher {

	private final JavaCipher cipher = JAVA_CIPHER.BLOWFISH.getInstance();


	@Override
	protected I_Object call3( final CallRuntime cr, final String method ) {
		return null;
	}

	@Override
	protected JavaCipher getCipher() {
		return this.cipher;
	}

}
