/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt.cipher;

import de.mn77.lib.crypt.cipher.I_RotStringCipher;


/**
 * @author Michael Nitsche
 * @created 04.12.2023
 * @apiNote This type is an easter egg and is just for fun :-P
 */
public class JMo_CipherRot26 extends A_RotCipher {

	private final I_RotStringCipher cipher = new I_RotStringCipher() {

		public String compute( final String data ) {
			return data;
		}

		public byte[] decrypt( final byte[] data, final byte[] key ) {
			return data;
		}

		public byte[] encrypt( final byte[] data, final byte[] key ) {
			return data;
		}

		public int keyLengthMax() {
			return 0;
		}

		public int keyLengthMin() {
			return 0;
		}

	};

	@Override
	protected I_RotStringCipher getCipher() {
		return this.cipher;
	}

}
