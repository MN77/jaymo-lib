/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt.hash;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.crypt.A_Digest;
import org.jaymo_lang.lib.crypt.Util_Crypt;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.lib.crypt.digest.JAVA_DIGEST;
import de.mn77.lib.crypt.digest.JavaDigest;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public class JMo_HashMD5 extends A_Digest {

	private final JavaDigest digest = new JavaDigest( JAVA_DIGEST.MD5 );


	@Override
	protected byte[] compute() {
		return this.digest.compute();
	}

	@Override
	protected void pAdd( final CallRuntime cr, final I_Object obj ) {
		final Object o = Util_Crypt.add( cr, obj );
		if( o instanceof Byte )
			this.digest.add( (byte)o );
		else if( o instanceof byte[] )
			this.digest.add( (byte[])o );
		else
			throw new RuntimeError( cr, "Invalid data type", "Allowed are Byte, ByteArray and Str, got: " + obj.getTypeName() );
	}

}
