/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 15.08.2022
 */
public abstract class A_BaseCodec extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "encode":
				return this.mEncode( cr );

			case "decode":
//			case "decodeToBytes":
				return this.mDecode( cr );
		}
		return null;
	}

	protected abstract byte[] pDecode( byte[] ba );

	protected abstract byte[] pDecode( String s );

	protected abstract String pEncode( byte[] ba );

	protected abstract String pEncode( String s );

	/**
	 * °decode(ByteArray|Str data)ByteArray # Decode a encoded ByteArray or a String.
	 */
	private I_Object mDecode( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_ByteArray.class, JMo_Str.class );

		if( arg instanceof JMo_Str ) {
			final String s = Lib_Convert.toStr( cr, arg ).rawString();
			return new JMo_ByteArray( this.pDecode( s ) );
		}
		else {
			final JMo_ByteArray jba = (JMo_ByteArray)Lib_Convert.toValue( cr, arg );
			return new JMo_ByteArray( this.pDecode( jba.getValue() ) );
		}
	}

	/**
	 * °encode(ByteArray|Str data)Str # Encode a ByteArray or a String.
	 */
	private I_Object mEncode( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_ByteArray.class, JMo_Str.class );

		if( arg instanceof JMo_Str ) {
			final String s = Lib_Convert.toStr( cr, arg ).rawString();
//			return new JMo_ByteArray(this.pEncode(s));
			return new JMo_Str( this.pEncode( s ) );
		}
		else {
			final JMo_ByteArray jba = (JMo_ByteArray)Lib_Convert.toValue( cr, arg );
//			return new JMo_ByteArray(Lib_Base64.encode(jba.getValue()));
			return new JMo_Str( this.pEncode( jba.getValue() ) );
		}
	}

}
