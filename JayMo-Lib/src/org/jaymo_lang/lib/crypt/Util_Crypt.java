/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.crypt;

import java.nio.charset.StandardCharsets;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public class Util_Crypt {

	private Util_Crypt() {}

	
	/**
	 * @return A Byte or a byte[];
	 */
	public static Object add( final CallRuntime cr, final I_Object arg ) {
		if( arg instanceof JMo_Byte argByte)
			return argByte.rawObject();
		else if( arg instanceof JMo_ByteArray )
			return ((JMo_ByteArray)arg).getValue();
		else if( arg instanceof JMo_Str argStr) {
			final String s = argStr.rawString();
			return s.getBytes( StandardCharsets.UTF_8 );
		}
		else if( arg instanceof I_Atomic )
			Err.todo( arg );
		throw Err.invalid( arg, arg.getClass().getSimpleName() );
	}

}
