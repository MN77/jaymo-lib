/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jaymo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.json.Util_Json;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_DecNumber;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.immute.datetime.JMo_Date;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.Sys;
import de.mn77.lib.graphic.I_ImageX;
import de.mn77.lib.graphic.MImageX;
import de.mn77.lib.json.Json;


/**
 * @author Michael Nitsche
 *
 */
public class JMo_BrickLinkAPI extends A_ObjectSimple {

	private static final boolean DEBUG = false;

	private final Map<String, String> httpPropertys  = new HashMap<>();
	private String                    proxyURL       = null;
	private String                    proxyKey       = null;
	private int                       blCallsTotal   = 0;
	private int                       blCallsSuccess = 0;
	private Integer                   limit          = null;

	private final BL_Auth auth = new BL_Auth();


	public Object bricklinkGet( final CallRuntime cr, final BL_METHOD method, final String url, final BL_Auth auth, final Map<String, String> uri_props, final Map<String, Object> request_props ) {
		final int TRIES = 15; // 5 kommt vor, 10 selten aber auch schon passiert	// 15 sollte ok sein

		ExternalError err = null;

		for( byte t = 1; t <= TRIES; t++ ) { // TODO HACK!!! This is generally a bad solution. Find the real bug and fix it!
			if( this.limit != null && this.blCallsTotal >= this.limit )
				throw new ExternalError( cr, "Bricklink-Call-Limit reached", this.limit + " times tried and failed. Aborting!" );

			this.blCallsTotal++;

			try {
				final String bl_url = Util_Bricklink.getBricklinkUrl( cr, method, url, auth, uri_props );
//				MOut.temp(bl_url);
				final HttpURLConnection connection = this.iGetConnection( cr, method, bl_url, request_props );
				final String result = this.iRead( cr, connection );
//				MOut.temp(result);
				this.blCallsSuccess++;

				return this.iParseAndCheck( cr, result );
			}
			catch( final ExternalError e ) {
				err = e;
				if( JMo_BrickLinkAPI.DEBUG )
					Err.show( err );
				if( !e.getDetail().startsWith( "401: BAD_OAUTH_REQUEST / SIGNATURE_INVALID" ) )
					throw e;
				Sys.sleepSeconds( 1 );
			}
		}

		throw err;
	}

	/**
	 * °calls ^ getCallsTotal
	 * °callsTotal()Int # Returns the total amount of calls to BrickLink.
	 * °callsSuccess()Int # Returns the amount of successful calls to BrickLink.
	 */
	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			// General //
			case "setAuth":
				this.mSetAuth( cr );
				return this;
			case "setConsumer":
				this.mSetConsumer( cr );
				return this;
			case "setToken":
				this.mSetToken( cr );
				return this;
			case "addHttpProperty":
				this.mAddHttpProperty( cr );
				return this;
			case "setProxy":
				this.mSetProxy( cr );
				return this;
			case "setLimit":
				this.mSetLimit( cr );
				return this;

			case "calls":
			case "callsTotal":
				cr.argsNone();
				return new JMo_Int( this.blCallsTotal );
			case "callsSuccess":
				cr.argsNone();
				return new JMo_Int( this.blCallsSuccess );

			// Orders //
			case "fetchOrdersIn":
				return this.mGetOrders( cr, true );
			case "fetchOrdersOut":
				return this.mGetOrders( cr, false );
			case "fetchOrder":
				return this.mGetOrder( cr );
			case "fetchOrderItems":
				return this.mGetOrderItems( cr );
			case "fetchOrderMessages":
				return this.mGetOrderMessages( cr );
			case "fetchOrderFeedback":
				return this.mGetOrderFeedback( cr );
			case "updateOrder":
				return this.mUpdateOrder( cr );
			case "updateOrderStatus":
				return this.mUpdateOrderStatus( cr );
			case "updateOrderPayment":
				return this.mUpdateOrderPaymentStatus( cr );
			case "orderSendDriveThru":
				return this.mOrderSendDriveThru( cr );

			// Inventories //
			case "fetchInventories":
				return this.mFetchInventories( cr );
			case "fetchInventory":
				return this.mFetchInventory( cr );
			case "createInventory":
				return this.mCreateInventory( cr );
//			case "createInventories":	// TODO Not yet
			case "updateInventory":
				return this.mUpdateInventory( cr );
			case "deleteInventory":
				return this.mDeleteInventory( cr );

			// Items //
			case "fetchItem":
				return this.mFetchItem( cr );
			case "fetchItemImage":
			case "fetchImage":
				return this.mFetchImage( cr );
			case "fetchSupersets":
			case "fetchSuperSets":
				return this.mFetchSuperSets( cr );
			case "fetchSubsets":
			case "fetchSubSets":
				return this.mFetchSubSets( cr );
			case "fetchPriceGuide":
				return this.mFetchPriceGuide( cr );
			case "fetchKnownColors":
				return this.mFetchKnownColors( cr );
			case "fetchKnownColorValues":
				return this.mFetchKnownColorValues( cr );
			case "fetchThumbnailURL":
				return this.mFetchThumbnail( cr, false );
			case "fetchThumbnail":
				return this.mFetchThumbnail( cr, true );

			// Feedbacks //
//			case "fetchFeedbacks":
//			case "fetchFeedbackList":

			case "fetchFeedbacksIn":
				return this.mFetchFeedbacks( cr, true );
			case "fetchFeedbacksOut":
				return this.mFetchFeedbacks( cr, false );
			case "fetchFeedback":
				return this.mFetchFeedback( cr );
			case "postFeedback":
				return this.mPostFeedback( cr );
			case "replyFeedback":
				return this.mReplyFeedback( cr );

			// Colors //
			case "fetchColors":
			case "fetchColorList":
				return this.mFetchColors( cr );
			case "fetchColor":
				return this.mFetchColor( cr );

			// Categories //
			case "fetchCategories":
				return this.mFetchCategories( cr );
			case "fetchCategory":
				return this.mFetchCategory( cr );

			// PushNotifications // TODO Not yet

			// Coupons //
			case "fetchCouponsIn":
				return this.mFetchCoupons( cr, true );
			case "fetchCouponsOut":
				return this.mFetchCoupons( cr, false );
			case "fetchCoupon":
				return this.mFetchCoupon( cr );
			case "createCoupon":
				return this.mCreateCoupon( cr );
//			case "updateCoupon":
//				return this.updateCoupon(cr);	// TODO Server problems
			case "deleteCoupon":
				return this.mDeleteCoupon( cr );

			// Settings //
			case "fetchShippingMethods":
			case "fetchShippingMethodList":
				return this.mFetchShippingMethodList( cr );
			case "fetchShippingMethod":
				return this.mFetchShippingMethod( cr );

			// Members //
			case "fetchMemberRating":
				return this.mFetchMemberRating( cr );
//			case "fetchMemberNote":		// TODO Not yet
//			case "createMemberNote":	// TODO Not yet
//			case "updateMemberNote":	// TODO Not yet
//			case "deleteMemberNote":	// TODO Not yet

			// Item mapping //
//			case "fetchElementID":	// TODO Not yet
//			case "fetchItemNumber":	// TODO Not yet

			default:
				return null;
		}
	}

	private HttpURLConnection iGetConnection( final CallRuntime cr, final BL_METHOD method, final String url, final Map<String, Object> request_props ) {
		final boolean useProxy = this.proxyURL != null;

		try {
			final URL server = new URL( useProxy ? this.proxyURL : url );
			final HttpURLConnection connection = (HttpURLConnection)server.openConnection();

			for( final Map.Entry<String, String> entry : this.httpPropertys.entrySet() )
				connection.setRequestProperty( entry.getKey(), entry.getValue() );

			if( useProxy )
				connection.setRequestProperty( this.proxyKey, url );

			if( method != BL_METHOD.GET ) {
				connection.setRequestMethod( method.getString() ); // GET, POST, PUT, DELETE, ...
				connection.setRequestProperty( "content-type", "application/json" );
//				connection.setRequestProperty("Content-Type", "application/json; utf-8");
//				connection.setRequestProperty("content-length", "18");

				connection.setDoOutput( true );
//				MOut.temp( connection.getHeaderFields() );
//				MOut.temp(connection.getRequestProperties());

//				con.setRequestProperty("Accept", "application/json");

				//-------------

//				final HashMap<String, Object> obj = new HashMap<>();
//				if(request_props != null)
//					for(final Map.Entry<String, String> e : request_props.entrySet())
//						obj.put(e.getKey(), e.getValue());
//				final String jsonInputString = obj.toString();

				final String jsonInputString = request_props == null ? null : Json.serialize( request_props );
//				MOut.temp(jsonInputString, jsonInputString == null ? "null" : jsonInputString.length());	// "{"remarks":"Foo1"}"		18
//				MOut.temp(connection);

				//-------------

				if( jsonInputString != null ) {
					final OutputStreamWriter out = new OutputStreamWriter( connection.getOutputStream() );
					out.write( jsonInputString );
					out.close();
				}

//				connection.getInputStream();
			}

			return connection;
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "Connection error", e.getMessage() );
		}
	}

	@SuppressWarnings( "unchecked" )
	private String iGetImageURL( final CallRuntime cr, final String type, final String no, final int color_id ) {
		final String urlGet = "/items/" + type + "/" + no + "/images/" + color_id;
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, null, null );
//		MOut.temp(json);
		return Util_Json.jsonToString( (HashMap<String, Object>)json, "thumbnail_url" );
	}

	@SuppressWarnings( "unchecked" )
	private Object iParseAndCheck( final CallRuntime cr, final String result ) {
		HashMap<String, Object> jsonObject;

		try {
			jsonObject = (HashMap<String, Object>)Json.parse( result );
		}
		catch( final ParseException e ) {
			throw new ExternalError( cr, "BrickLink JSON-Parse-Error", e.getMessage() );
		}

//		MOut.temp( jsonObject.keySet(), jsonObject.get( "meta" ) );

		final HashMap<String, Object> meta = (HashMap<String, Object>)jsonObject.get( "meta" );
		final long resultCode = (long)meta.get( "code" );
		if( resultCode == 404 ) // RESOURCE NOT FOUND
//			return null;
			throw new ExternalError( cr, "BrickLink Error", "404: Resource not found" ); // Bugfix: .getPriceGuide

		if( resultCode < 200 || resultCode > 299 )
			throw new ExternalError( cr, "BrickLink-Error", resultCode + ": " + meta.get( "message" ) + " / " + meta.get( "description" ) ); //TODO

//		I_Object result = this.jsonToMap(cr, jsonObject.get("data"));
		return jsonObject.get( "data" );
	}

	private String iRead( final CallRuntime cr, final HttpURLConnection connection ) {

		try {
			connection.connect();

			final InputStream is = connection.getInputStream();
			final Reader r = new InputStreamReader( is );
			final BufferedReader br = new BufferedReader( r );

			final StringBuilder sb = new StringBuilder();
			for( int i; (i = br.read()) != -1; )
				sb.append( (char)i );

			br.close();
			connection.disconnect();
			return sb.toString();
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "Connection error", e.getMessage() );
		}
	}

	/**
	 * °addHttpProperty(Str key, Str value)Same # Add a HTTP-Property to the next request. [Experimental]
	 */
	private void mAddHttpProperty( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String key = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String val = Lib_Convert.toStr( cr, args[1] ).rawString();
		this.httpPropertys.put( key, val );
	}


	/// ORDERS ///

	/**
	 * °createCoupon(Map data) Map # Create a coupon.
	 */
	private I_Object mCreateCoupon( final CallRuntime cr ) {
		final JMo_Map requestData = (JMo_Map)cr.args( this, JMo_Map.class )[0];

		final Map<String, Object> requestBody = new HashMap<>();
		for( final Entry<I_Object, I_Object> e : requestData.copyToHashMap().entrySet() )
			requestBody.put( Lib_Convert.toStr( cr, e.getKey() ).rawString(), Lib_Java.jmoToJava( cr, e.getValue(), null ) );

		final Object json = this.bricklinkGet( cr, BL_METHOD.POST, "/coupons", this.auth, null, requestBody );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °createInventory(Map properties)Map # Create a new inventory.
	 */
	private I_Object mCreateInventory( final CallRuntime cr ) {
		final JMo_Map requestData = (JMo_Map)cr.args( this, JMo_Map.class )[0];

		final ItemCreateData itemData = new ItemCreateData();
		itemData.fill( cr, requestData.copyToHashMap() );
		itemData.check( cr );
		final Map<String, String> requestProps = itemData.getMap();
		final Map<String, Object> requestProps2 = Util_Bricklink.convertDotToTree( requestProps );

		final Object json = this.bricklinkGet( cr, BL_METHOD.POST, "/inventories", this.auth, null, requestProps2 );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °deleteCoupon(IntNumber couponId) Object # Delete a coupon. [Experimental]
	 *
	 * TODO Not testet
	 */
	private I_Object mDeleteCoupon( final CallRuntime cr ) {
		final I_Object args = cr.args( this, I_IntNumber.class )[0];
		final String id = Lib_Convert.toStr( cr, args ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.DELETE, "/coupons/" + id, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °deleteInventory(IntNumber lot)Map # Delete a single inventory by 'lot' number.
	 */
	private I_Object mDeleteInventory( final CallRuntime cr ) {
		final I_Object args = cr.args( this, I_IntNumber.class )[0];
		final String id = Lib_Convert.toStr( cr, args ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.DELETE, "/inventories/" + id, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchCategories() Table # Fetch a table with (category_id, parent_id, category_name).
	 */
	@SuppressWarnings( "unchecked" )
	private I_Object mFetchCategories( final CallRuntime cr ) {
		cr.argsNone();
		final String urlGet = "/categories";
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, null, null );
//		return Lib_Json.jsonToJMo(cr, json);

		final ArrayTable<I_Object> tab = new ArrayTable<>( 3 );
		final List<?> arr = (List<?>)json;

		for( final Object item : arr )
			tab.addRow(
				Util_Json.jsonToJMo( ((HashMap<String, Object>)item).get( "category_id" ) ),
				Util_Json.jsonToJMo( ((HashMap<String, Object>)item).get( "parent_id" ) ),
				Util_Json.jsonToJMo( ((HashMap<String, Object>)item).get( "category_name" ) ) );
		return new JMo_Table( tab );
	}

	/**
	 * °fetchCategory(IntNumber id) Map # Fetch informations about a category.
	 */
	private I_Object mFetchCategory( final CallRuntime cr ) {
		final I_Object par1 = cr.args( this, I_IntNumber.class )[0];
		final int id = Lib_Convert.toInt( cr, par1 );
		final String urlGet = "/categories/" + id;
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchColor(IntNumber colorId) Map # Fetch informations about a color.
	 */
	private I_Object mFetchColor( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class );
		final String no = Lib_Convert.toStr( cr, args[0] ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/colors/" + no, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchColors ^ getColorList
	 * °fetchColorList() List # Fetch a list with all available colors.
	 */
	private I_Object mFetchColors( final CallRuntime cr ) {
		cr.argsNone();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/colors", this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchCoupon(IntNumber couponID) Map # Fetch informations about a coupon.
	 */
	private I_Object mFetchCoupon( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final String no = Lib_Convert.toStr( cr, arg ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/coupons/" + no, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}


	/// Inventory ///

	/**
	 * °fetchCouponsIn() List # Fetch a list of all incoming coupons.
	 * °fetchCouponsIn(Str status) List # Fetch a list of all incoming coupons with given status.
	 * °fetchCouponsOut() List # Fetch a list of all outgoing coupons.
	 * °fetchCouponsOut(Str status) List # Fetch a list of all outgoing coupons with given status.
	 */
	private I_Object mFetchCoupons( final CallRuntime cr, final boolean in ) {
		final I_Object[] args = cr.argsFlex( this, 0, 1 );
		final Map<String, String> urlProps = new HashMap<>();

		urlProps.put( "direction", in ? "in" : "out" );

		if( args.length == 1 ) {
			final BricklinkIncExc statusIE = new BricklinkIncExc( cr, args[0] );
			statusIE.check( value -> {
				return Util_Bricklink.checkCouponState( cr, value );
			} );

			if( statusIE.hasItems() )
//				urlProps.put("status", ConvertSequence.toString(",", statusIE.compute()));
				urlProps.put( "status", statusIE.compute() );
		}

		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/coupons", this.auth, urlProps, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchFeedback(IntNumber feedbackID) Map # Fetch a single feedback.
	 */
	private I_Object mFetchFeedback( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final String no = Lib_Convert.toStr( cr, arg ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/feedback/" + no, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchFeedbacksIn() List # Fetch incoming feedbacks
	 * °fetchFeedbacksOut() List # Fetch sent feedbacks
	 */
	private I_Object mFetchFeedbacks( final CallRuntime cr, final boolean in ) {
		cr.argsNone();
		final Map<String, String> urlProps = new HashMap<>();
		urlProps.put( "direction", in ? "in" : "out" );
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/feedback", this.auth, urlProps, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchImage ^ getItemImage
	 * °fetchItemImage(Str type, Atomic no) Image # Fetch the image of an item.
	 * °fetchItemImage(Str type, Atomic no, IntNumber color) Image # Fetch the image of an item in the selected color.
	 */
	private JMo_Image mFetchImage( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 2, 3 );
		final String type = Lib_Convert.toStr( cr, cr.argType( args[0], JMo_Str.class ) ).rawString();
		final String no = Lib_Convert.toStr( cr, cr.argType( args[1], I_Atomic.class ) ).rawString();
		final Integer color = args.length == 2 || args[2] == Nil.NIL ? null : Lib_Convert.toInt( cr, cr.argType( args[2], I_IntNumber.class ) );
		final String urlString = Util_Bricklink.getImageURL( type, no, color );

		if( urlString == null )
			throw new ExternalError( cr, "Image URL error", "Not implemented type of item. Type: " + type + ", No: " + no + ", Color: " + color );
		// TODO Try to request from Bricklink item

		try {
			final URL url = new URL( urlString );
			final BufferedImage awtImage = ImageIO.read( url ); // Download image
			if( awtImage == null )
				throw new ExternalError( cr, "Image download error", "Can't download image from URL: " + url );
			final I_ImageX mImage = new MImageX( awtImage );
			return new JMo_Image( mImage );
		}
		catch( final ErrorBaseDebug | Err_Runtime e ) {
			throw e;
		}
		catch( final Exception e ) {
			Err.show( e );
			throw new ExternalError( cr, "Image download error", e.getMessage() ); // TODO
		}
	}

	/**
	 * °fetchInventories()List # Fetch a list with all inventories.
	 * °fetchInventories(Map filter)List # Fetch a list with all inventories, which match the given filter.
	 * TODO Testen
	 */
	private I_Object mFetchInventories( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 0, 1 );
		Map<String, String> params = null;

		if( args.length == 1 ) {
			params = new HashMap<>();

			final JMo_Map arg = cr.argType( args[0], JMo_Map.class );

			for( final Entry<I_Object, I_Object> e : arg.copyToHashMap().entrySet() ) {
				final String key = cr.argType( e.getKey(), JMo_Str.class ).toString().toLowerCase();

				switch( key ) {
					case "item_type":
						final BricklinkIncExc itIE1 = new BricklinkIncExc( cr, e.getValue() );
						itIE1.check( value -> {
							return Util_Bricklink.checkItemType( cr, value );
						} );
						params.put( "item_type", itIE1.compute() );
						break;
					case "status":
						final BricklinkIncExc itIE2 = new BricklinkIncExc( cr, e.getValue() );
						itIE2.check( value -> {
							return Util_Bricklink.checkInventoryStatus( cr, value );
						} );
						params.put( "status", itIE2.compute() );
						break;
					case "category_id":
						final BricklinkIncExc itIE3 = new BricklinkIncExc( cr, e.getValue() );
						itIE3.check( value -> {
							return "" + Util_Bricklink.checkInteger( cr, value );
						} );
						params.put( "category_id", itIE3.compute() );
						break;
					case "color_id":
						final BricklinkIncExc itIE4 = new BricklinkIncExc( cr, e.getValue() );
						itIE4.check( value -> {
							return "" + Util_Bricklink.checkInteger( cr, value );
						} );
						params.put( "color_id", itIE4.compute() );
						break;

					default:
				}
			}
		}

//		MOut.temp(params);
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/inventories", this.auth, params, null );
		return Util_Json.jsonToJMo( json );
	}


	/// Items ///

	/**
	 * °fetchInventory(IntNumber lot)Map # Fetch a single inventory by 'lot' number.
	 */
	private I_Object mFetchInventory( final CallRuntime cr ) {
		final I_Object args = cr.args( this, I_IntNumber.class )[0];
		final String id = Lib_Convert.toStr( cr, args ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/inventories/" + id, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchItem(Str type, Atomic no)Map # Fetch item information by 'type' (like 'PART') and 'number' (like '3001').
	 */
	private I_Object mFetchItem( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, I_Atomic.class );
		final String type = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String no = Lib_Convert.toStr( cr, args[1] ).rawString();

		final String type2 = Util_Bricklink.checkItemType( cr, type );
		final String urlGet = "/items/" + type2 + "/" + no;
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchKnownColors(Str type, Atomic no)List # Fetch a list of all known colors of the given item.
	 */
	@SuppressWarnings( "unchecked" )
	private I_Object mFetchKnownColors( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, I_Atomic.class );
		final String type = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String no = Lib_Convert.toStr( cr, args[1] ).rawString();

		final String urlGet = "/items/" + type + "/" + no + "/colors";
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, null, null );

		final List<?> arr = (List<?>)json;
		final SimpleList<I_Object> al = new SimpleList<>( arr.size() );

		for( final Object item : arr )
			al.add( Util_Json.jsonToJMo( ((HashMap<String, Object>)item).get( "color_id" ) ) );
		return new JMo_List( al );
	}

	/**
	 * °fetchKnownColorValues(Str type, Atomic no) Object # Fetch a table with all known colors and values of the given item.
	 */
	private I_Object mFetchKnownColorValues( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, I_Atomic.class );
		final String type = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String no = Lib_Convert.toStr( cr, args[1] ).rawString();

		final String urlGet = "/items/" + type + "/" + no + "/colors";
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchMemberRating(Str member) Map # Fetch the rating of a member.
	 */
	private I_Object mFetchMemberRating( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String mid = Lib_Convert.toStr( cr, arg ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/members/" + mid + "/ratings", this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchPriceGuide(Str type, Atomic no, IntNumber color_id, Bool new_or_used, Bool, sold_or_stock, Map propertys)Map # Fetch the price guide information of a item.
	 */
	@SuppressWarnings( "unchecked" )
	private I_Object mFetchPriceGuide( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, I_Atomic.class, I_IntNumber.class, JMo_Bool.class, JMo_Bool.class, JMo_Map.class );
		final String type = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String no = Lib_Convert.toStr( cr, args[1] ).rawString();
		final String color_id = Lib_Convert.toStr( cr, args[2] ).rawString();
		final String new_or_used = Lib_Convert.toBoolean( cr, args[3] ) ? "N" : "U";
		final String sold_or_stock = Lib_Convert.toBoolean( cr, args[4] ) ? "sold" : "stock";
		final Map<I_Object, I_Object> props = ((JMo_Map)args[5]).copyToHashMap();

		final String urlGet = "/items/" + type + "/" + no + "/price";

		final HashMap<String, String> propertys = new HashMap<>();
		propertys.put( "color_id", color_id );
		propertys.put( "new_or_used", new_or_used );
		propertys.put( "guide_type", sold_or_stock );

		for( final Entry<I_Object, I_Object> entry : props.entrySet() )
			propertys.put( Lib_Convert.toStr( cr, entry.getKey() ).rawString(), Lib_Convert.toStr( cr, entry.getValue() ).rawString() );

		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, propertys, null );
		return Util_Json.jsonToMap( (HashMap<String, Object>)json, "max_price", "min_price", "qty_avg_price", "total_quantity", "avg_price", "unit_quantity" ); //, "currency_code"
	}

	/**
	 * °fetchShippingMethod(IntNumber methodId) Map # Fetch informations about a shipping method.
	 */
	private I_Object mFetchShippingMethod( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final String mid = Lib_Convert.toStr( cr, arg ).rawString();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/settings/shipping_methods/" + mid, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchShippingMethods ^ getShippingMethodList
	 * °fetchShippingMethodList() List # Fetch a list with all shipping methods.
	 */
	private I_Object mFetchShippingMethodList( final CallRuntime cr ) {
		cr.argsNone();
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/settings/shipping_methods", this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchSubsets ^ getSubSets
	 * °fetchSubSets(Str type, Atomic no) List # Fetch all sub sets.
	 * °fetchSubSets(Str type, Atomic no, IntNumber colorId) List # Fetch all sub sets.
	 * °fetchSubSets(Str type, Atomic no, IntNumber colorId, Bool withBox, Bool withInstruction, Bool breakMinifigs, Bool breakSubSets) List # Fetch all sub sets.
	 */
	private I_Object mFetchSubSets( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 2, 7 );
		final String type = Lib_Convert.toStr( cr, cr.argType( args[0], JMo_Str.class ) ).rawString();
		final String no = Lib_Convert.toStr( cr, cr.argType( args[1], I_Atomic.class ) ).rawString();

		final Map<String, String> uriProps = new HashMap<>();

		if( args.length >= 3 ) {
			final String color = Lib_Convert.toStr( cr, cr.argType( args[2], I_IntNumber.class ) ).rawString();
			uriProps.put( "color_id", color );
		}

		if( args.length >= 4 ) {
			final String color = Lib_Convert.toStr( cr, cr.argType( args[3], JMo_Bool.class ) ).rawString();
			uriProps.put( "box", color );
		}

		if( args.length >= 5 ) {
			final String color = Lib_Convert.toStr( cr, cr.argType( args[4], JMo_Bool.class ) ).rawString();
			uriProps.put( "instruction", color );
		}

		if( args.length >= 6 ) {
			final String color = Lib_Convert.toStr( cr, cr.argType( args[5], JMo_Bool.class ) ).rawString();
			uriProps.put( "break_minifigs", color );
		}

		if( args.length == 7 ) {
			final String color = Lib_Convert.toStr( cr, cr.argType( args[6], JMo_Bool.class ) ).rawString();
			uriProps.put( "break_subsets", color );
		}

		final String type2 = Util_Bricklink.checkItemType( cr, type );
		final String urlGet = "/items/" + type2 + "/" + no + "/subsets";
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, uriProps, null );

		return Util_Json.jsonToJMo( json );
	}


	/// Feedback ///

	/**
	 * °fetchSupersets ^ getSuperSets
	 * °fetchSuperSets(Str type, Atomic no) List # Fetch a list with all super sets of the given item.
	 * °fetchSuperSets(Str type, Atomic no, IntNumber colorId) List # Fetch a list with all super sets of the given item and color.
	 */
	private I_Object mFetchSuperSets( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 2, 3 );
		final String type = Lib_Convert.toStr( cr, cr.argType( args[0], JMo_Str.class ) ).rawString();
		final String no = Lib_Convert.toStr( cr, cr.argType( args[1], I_Atomic.class ) ).rawString();
		Map<String, String> uriProps = null;

		if( args.length == 3 ) {
			final String color = Lib_Convert.toStr( cr, cr.argType( args[2], I_IntNumber.class ) ).rawString();
			uriProps = new HashMap<>();
			uriProps.put( "color_id", color );
		}

		final String type2 = Util_Bricklink.checkItemType( cr, type );
		final String urlGet = "/items/" + type2 + "/" + no + "/supersets";
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, urlGet, this.auth, uriProps, null );

		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchThumbnail(Str type, Str no, IntNumber color_id) Image # Fetch a thumbnail image.
	 * °fetchThumbnailURL(Str type, Str no, IntNumber color_id) Str # Fetch the URL of a thumbnail image.
	 *
	 * TODO Abfrage über Bricklink überhaupt nötig?
	 * Format: //img.bricklink.com/P/11/3001.jpg
	 */
	private I_Object mFetchThumbnail( final CallRuntime cr, final boolean download ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class, I_IntNumber.class );
		final String type = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String no = Lib_Convert.toStr( cr, args[1] ).rawString();
		final int color_id = Lib_Convert.toInt( cr, args[2] );

		final String urlString = "https:" + this.iGetImageURL( cr, type, no, color_id );

		if( !download )
			return new JMo_Str( urlString );

		Image awtImage = null;

		try {
			final URL url = new URL( urlString );
			awtImage = ImageIO.read( url ); // Download image
			final I_ImageX mImage = new MImageX( awtImage );
			return new JMo_Image( mImage );
		}
		catch( final Exception e ) {
			throw new ExternalError( cr, "Image download error - " + e.getMessage(), "URL: " + urlString );
		}
	}

	/**
	 * °fetchOrder(IntNumber orderId)Map # Fetch order informations.
	 */
	private I_Object mGetOrder( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final int order_id = Lib_Convert.toInt( cr, arg );
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/orders/" + order_id, this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °fetchOrderFeedback(IntNumber orderId)List # Fetch feedback to order.
	 */
	private I_Object mGetOrderFeedback( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final int order_id = Lib_Convert.toInt( cr, arg );
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/orders/" + order_id + "/feedback", this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}


	/// Colors ///

	/**
	 * °fetchOrderItems(IntNumber orderId)List # Fetch a list of all order items.
	 */
	private I_Object mGetOrderItems( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final int order_id = Lib_Convert.toInt( cr, arg );
		final String url = "/orders/" + order_id + "/items";

		final ArrayList<?> json = (ArrayList<?>)this.bricklinkGet( cr, BL_METHOD.GET, url, this.auth, null, null );

		if( json.size() == 0 )
			return new JMo_List();
		else {
			final Object json2 = json.get( 0 );
			return Util_Json.jsonToJMo( json2 );
		}
	}

	/**
	 * °fetchOrderMessages(IntNumber orderId)List # Fetch all order messages.
	 */
	private I_Object mGetOrderMessages( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		final int order_id = Lib_Convert.toInt( cr, arg );
		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/orders/" + order_id + "/messages", this.auth, null, null );
		return Util_Json.jsonToJMo( json );
	}


	/// Categories ///

	/**
	 * °fetchOrdersIn()List # Fetch all incoming orders.
	 * °fetchOrdersIn(Bool filed)List # Fetch incoming orders, but only filed or not filed.
	 * °fetchOrdersIn(Bool filed, Object status)List # Fetch incoming orders that match to the given arguments.
	 * °fetchOrdersOut()List # Fetch all outgoing orders.
	 * °fetchOrdersOut(Bool filed)List # Fetch outgoing orders, but only filed or not filed.
	 * °fetchOrdersOut(Bool filed, Object status)List # Fetch outgoing orders that match to the given arguments.
	 */
	private I_Object mGetOrders( final CallRuntime cr, final boolean in ) {
		final I_Object[] arg = cr.argsFlex( this, 0, 2 );
		final Map<String, String> props = new HashMap<>();

		props.put( "direction", in ? "in" : "out" );

		final boolean filed = arg.length == 0 ? false : Lib_Convert.toBoolean( cr, cr.argType( arg[0], JMo_Bool.class ) );
		props.put( "filed", filed ? "true" : "false" );

		if( arg.length == 2 ) {
			final BricklinkIncExc statusIE = new BricklinkIncExc( cr, arg[1] );
			statusIE.check( value -> {
				return Util_Bricklink.checkOrderState( cr, value );
			} );

			if( statusIE.hasItems() )
//				props.put("status", ConvertSequence.toString(",", statusIE.compute()));
				props.put( "status", statusIE.compute() );
		}

		final Object json = this.bricklinkGet( cr, BL_METHOD.GET, "/orders", this.auth, props, null );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °orderSendDriveThru(IntNumber orderId, Bool mailMe)List # Send 'DriveThru'-Email of a order.
	 */
	private I_Object mOrderSendDriveThru( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class, JMo_Bool.class );
		final int order_id = Lib_Convert.toInt( cr, args[0] );
		final boolean mail_me = Lib_Convert.toBoolean( cr, args[1] );

		final Map<String, String> props = new HashMap<>();
		props.put( "mail_me", mail_me ? "true" : "false" );

		final Object json = this.bricklinkGet( cr, BL_METHOD.POST, "/orders/" + order_id + "/drive_thru", this.auth, props, null );
		return Util_Json.jsonToJMo( json );
	}


	/// Coupons ///

	/**
	 * °postFeedback(IntNumber orderID, IntNumber rating, Str comment) Map # Post feedback.
	 */
	private I_Object mPostFeedback( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class, I_IntNumber.class, JMo_Str.class );
		final String orderID = Lib_Convert.toStr( cr, args[0] ).rawString();
		final int rating = Lib_Convert.toInt( cr, args[1] );
		final String comment = Lib_Convert.toStr( cr, args[2] ).rawString();

		Lib_Error.ifNotBetween( cr, 0, 2, rating, "Rating" );
		Lib_Error.ifEmpty( cr, comment, "Comment" );

		final Map<String, Object> requestBody = new HashMap<>();
		requestBody.put( "order_id", orderID );
		requestBody.put( "rating", "" + rating );
		requestBody.put( "comment", comment );

		final Object json = this.bricklinkGet( cr, BL_METHOD.POST, "/feedback", this.auth, null, requestBody );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °replyFeedback(IntNumber feedbackID, Str reply) Object # Reply a feedback.
	 * TODO Return-Type?
	 */
	private I_Object mReplyFeedback( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class, JMo_Str.class );
		final String no = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String reply = Lib_Convert.toStr( cr, args[1] ).rawString();

		Lib_Error.ifEmpty( cr, reply, "Reply" );

		final Map<String, Object> requestBody = new HashMap<>();
		requestBody.put( "reply", reply );

		final Object json = this.bricklinkGet( cr, BL_METHOD.POST, "/feedback/" + no + "/reply", this.auth, null, requestBody );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °setAuth(Str consumerKey, Str consumerSecret, Str tokenValue, Str tokenSecret)Same # Set consumer keys and access token.
	 */
	private void mSetAuth( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class, JMo_Str.class, JMo_Str.class );
		final String consumerKey = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String consumerSecret = Lib_Convert.toStr( cr, args[1] ).rawString();
		final String tokenValue = Lib_Convert.toStr( cr, args[2] ).rawString();
		final String tokenSecret = Lib_Convert.toStr( cr, args[3] ).rawString();
		this.auth.setAuth( consumerKey, consumerSecret, tokenValue, tokenSecret );
	}

	/**
	 * #°updateCoupon(IntNumber couponId, Map data) Map
	 */
//	private I_Object updateCoupon(final CallRuntime cr) {
	// TODO Obviously okay, but got always a INTERNAL-SERVER-ERROR
//		final I_Object[] args = cr.args(this, I_IntNumber.class, JMo_Map.class);
//		final String id = Lib_Convert.getStringValue(cr, args[0]);
//		final JMo_Map requestBody = (JMo_Map)args[1];
//
//		final Map<String, Object> request_props = new HashMap<>();
//		for(final Map.Entry<I_Object, I_Object> e : requestBody.copyToHashMap().entrySet()) {
//			final String key = Lib_Convert.getStringValue(cr, e.getKey());
//			final String value = Lib_Convert.getStringValue(cr, e.getValue());
//			request_props.put(key, value);
//		}
//
//		final String uri = "/coupons/" + id;
//		final Object json = this.bricklinkGet(cr, BL_METHOD.PUT, uri, this.auth, null, request_props);
//		return Lib_Json.jsonToJMo(json);
//	}

	/**
	 * °setConsumer(Str key, Str secret)Same # Set the consumer access keys.
	 */
	private void mSetConsumer( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String consumerKey = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String consumerSecret = Lib_Convert.toStr( cr, args[1] ).rawString();
		this.auth.setConsumer( consumerKey, consumerSecret );
	}


	/// Settings ///

	/**
	 * °setLimit(IntNumber max)Same # Set a maximal connection limit.
	 */
	private void mSetLimit( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, I_IntNumber.class )[0];
		this.limit = Lib_Convert.toInt( cr, arg );
	}

	/**
	 * °setProxy(Str url, Str key)Same # Set Proxy configuration. [Experimental]
	 */
	private void mSetProxy( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String url = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String key = Lib_Convert.toStr( cr, args[1] ).rawString();
		this.proxyURL = url;
		this.proxyKey = key;
	}


	/// Member ///

	/**
	 * °setToken(Str value, Str secret)Same # Set the access token.
	 */
	private void mSetToken( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_Str.class );
		final String tokenValue = Lib_Convert.toStr( cr, args[0] ).rawString();
		final String tokenSecret = Lib_Convert.toStr( cr, args[1] ).rawString();
		this.auth.setToken( tokenValue, tokenSecret );
	}


	//----------------------------------------------------------

	/**
	 * °updateInventory(Atomic id, Map to_set)Map # Update a single inventory.
	 */
	private I_Object mUpdateInventory( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Atomic.class, JMo_Map.class );
		final String id = Lib_Convert.toStr( cr, args[0] ).rawString();
		final JMo_Map requestBody = (JMo_Map)args[1];

		final Map<String, Object> request_props = new HashMap<>();

		for( final Map.Entry<I_Object, I_Object> e : requestBody.copyToHashMap().entrySet() ) {
			final String key = Lib_Convert.toStr( cr, e.getKey() ).rawString();
			final String value = Lib_Convert.toStr( cr, e.getValue() ).rawString();
			request_props.put( key, value );
		}

		final String uri = "/inventories/" + id;
		final Object json = this.bricklinkGet( cr, BL_METHOD.PUT, uri, this.auth, null, request_props ); //, requestBody.internalTable()
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °updateOrder(IntNumber orderId, Map properties)List # Update the properties of a single order
	 */
	private I_Object mUpdateOrder( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class, JMo_Map.class );
		final int order_id = Lib_Convert.toInt( cr, args[0] );
		final JMo_Map requestBody = (JMo_Map)args[1];

		final Map<String, String> requestProps = new HashMap<>();

		for( final Map.Entry<I_Object, I_Object> e : requestBody.copyToHashMap().entrySet() ) {
			final String key = Lib_Convert.toStr( cr, e.getKey() ).rawString().toLowerCase();

			switch( key ) {
				case "shipping.date_shipped":
					requestProps.put( key, cr.argType( e.getValue(), JMo_Date.class ).toString() );
					break;
				case "cost.credit":
				case "cost.insurance":
				case "cost.etc1":
				case "cost.etc2":
				case "cost.shipping":
					requestProps.put( key, cr.argType( e.getValue(), I_DecNumber.class ).toString() );
					break;
				case "shipping.method_id":
					requestProps.put( key, cr.argType( e.getValue(), I_IntNumber.class ).toString() );
					break;
				case "shipping.tracking_no":
				case "shipping.tracking_link":
				case "remarks":
					requestProps.put( key, cr.argType( e.getValue(), JMo_Str.class ).toString() );
					break;
				case "is_filed":
					requestProps.put( key, cr.argType( e.getValue(), JMo_Bool.class ).toString() );
					break;

				default:
					throw new RuntimeError( cr, "Invalid key", "Key is not known: " + key );
			}
		}

		final Map<String, Object> requestProps2 = Util_Bricklink.convertDotToTree( requestProps );
		final Object json = this.bricklinkGet( cr, BL_METHOD.PUT, "/orders/" + order_id, this.auth, null, requestProps2 );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °updateOrderPayment(IntNumber orderId, Str status)List # Update the payment status of a single order.
	 */
	private I_Object mUpdateOrderPaymentStatus( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class, JMo_Str.class );
		final int order_id = Lib_Convert.toInt( cr, args[0] );
		String status = Lib_Convert.toStr( cr, args[1] ).rawString();

		status = Util_Bricklink.checkPaymentState( cr, status );

		final Map<String, Object> requestProps = new HashMap<>();
		requestProps.put( "field", "payment_status" );
		requestProps.put( "value", status );

		final Object json = this.bricklinkGet( cr, BL_METHOD.PUT, "/orders/" + order_id + "/status", this.auth, null, requestProps );
		return Util_Json.jsonToJMo( json );
	}

	/**
	 * °updateOrderStatus(IntNumber orderId, Str status)List # Update the status of a single order.
	 */
	private I_Object mUpdateOrderStatus( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_IntNumber.class, JMo_Str.class );
		final int order_id = Lib_Convert.toInt( cr, args[0] );
		String status = Lib_Convert.toStr( cr, args[1] ).rawString();

		status = Util_Bricklink.checkOrderState( cr, status );

		final Map<String, Object> requestProps = new HashMap<>();
		requestProps.put( "field", "status" );
		requestProps.put( "value", status );

		final Object json = this.bricklinkGet( cr, BL_METHOD.PUT, "/orders/" + order_id + "/status", this.auth, null, requestProps );
		return Util_Json.jsonToJMo( json );
	}

}
