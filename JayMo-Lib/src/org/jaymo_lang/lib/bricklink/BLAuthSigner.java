/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-Default-Library <https://www.jmo-lang.org>
 *
 * JayMo-Default-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-Default-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-Default-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.bricklink;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import de.mn77.lib.crypt.CodecBase64;


public class BLAuthSigner {

	static class OAuthEncoder {

		private static final Map<String, String> ENCODING_RULES;

		static {
			final Map<String, String> rules = new HashMap<>();
			rules.put( "*", "%2A" );
			rules.put( "+", "%20" );
			rules.put( "%7E", "~" );
			ENCODING_RULES = Collections.unmodifiableMap( rules );
		}


		public static String encode( final String plain ) throws UnsupportedEncodingException {
			String encoded = URLEncoder.encode( plain, BLAuthSigner.CHARSET );

			for( final Map.Entry<String, String> rule : OAuthEncoder.ENCODING_RULES.entrySet() )
				encoded = encoded.replaceAll( Pattern.quote( rule.getKey() ), rule.getValue() );
			return encoded;
		}

	}

	static class Timer {

		private final Random rand = new Random();


		Long getMilis() {
			return System.currentTimeMillis();
		}

		Integer getRandomInteger() {
			return this.rand.nextInt();
		}

	}


	private static final String CHARSET   = "UTF-8";
	private static final String HMAC_SHA1 = "HmacSHA1";

	private static final String EMPTY_STRING    = "";
	private static final String CARRIAGE_RETURN = "\r\n";
	private static final String signMethod      = "HMAC-SHA1";
	private static final String version         = "1.0";
	private final String        consumerKey;
	private final String        consumerSecret;

	private String tokenValue;
	private String tokenSecret;

	private String url;
	private String verb;

	private final Map<String, String> oauthParameters;


	private final Map<String, String> queryParameters;

	private final Timer timer;


	public BLAuthSigner( final String consumerKey, final String consumerSecret ) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.oauthParameters = new HashMap<>();
		this.queryParameters = new HashMap<>();
		this.timer = new Timer();
	}

	public void addParameter( final String key, final String value ) {
		this.queryParameters.put( key, value );
	}

	public String computeSignature() throws Exception {
		this.addOAuthParameter( OAuthConstants.VERSION, BLAuthSigner.version );
		this.addOAuthParameter( OAuthConstants.TIMESTAMP, this.getTimestampInSeconds() );
		this.addOAuthParameter( OAuthConstants.NONCE, this.getNonce() );
		this.addOAuthParameter( OAuthConstants.TOKEN, this.tokenValue );
		this.addOAuthParameter( OAuthConstants.CONSUMER_KEY, this.consumerKey );
		this.addOAuthParameter( OAuthConstants.SIGN_METHOD, BLAuthSigner.signMethod );

		final String baseString = this.getBaseString();
//		MOut.temp(baseString);
		final String keyString = OAuthEncoder.encode( this.consumerSecret ) + '&' + OAuthEncoder.encode( this.tokenSecret );
		return this.doSign( baseString, keyString );
	}

	public Map<String, String> getFinalOAuthParams() throws Exception {
		final String signature = this.computeSignature();

		final Map<String, String> params = new HashMap<>();
		params.putAll( this.oauthParameters );
		params.put( OAuthConstants.SIGNATURE, signature );

		return params;
	}

	public void setToken( final String tokenValue, final String tokenSecret ) {
		this.tokenValue = tokenValue;
		this.tokenSecret = tokenSecret;
	}

	public void setURL( final String url ) {
		this.url = url;
	}

	public void setVerb( final String verb ) {
		this.verb = verb;
	}

	private void addOAuthParameter( final String key, final String value ) {
		this.oauthParameters.put( key, value );
	}

	private String bytesToBase64String( final byte[] bytes ) throws Exception {
		return new String( CodecBase64.encode( bytes ), "UTF-8" );
	}

	private String doSign( final String toSign, final String keyString ) throws Exception {
		final SecretKeySpec key = new SecretKeySpec( keyString.getBytes( BLAuthSigner.CHARSET ), BLAuthSigner.HMAC_SHA1 );
		final Mac mac = Mac.getInstance( BLAuthSigner.HMAC_SHA1 );
		mac.init( key );
		final byte[] bytes = mac.doFinal( toSign.getBytes( BLAuthSigner.CHARSET ) );
		return this.bytesToBase64String( bytes ).replace( BLAuthSigner.CARRIAGE_RETURN, BLAuthSigner.EMPTY_STRING );
	}

	private String getBaseString() throws Exception {
		final List<String> params = new ArrayList<>();

		for( final Entry<String, String> entry : this.oauthParameters.entrySet() ) {
			final String param = OAuthEncoder.encode( entry.getKey() ).concat( "=" ).concat( entry.getValue() );
			params.add( param );
		}

		for( final Entry<String, String> entry : this.queryParameters.entrySet() ) {
			final String param = OAuthEncoder.encode( entry.getKey() ).concat( "=" ).concat( entry.getValue() );
			params.add( param );
		}

//		if(!this.verb.equals(BL_METHOD.GET.toString())) {
//			MOut.temp(this.verb);
//
//			String p=null;
//
//	//		String p1 = OAuthEncoder.encode("Content-Type").concat("=").concat("application/json; utf-8");
////			String p1 = OAuthEncoder.encode("Content-Type").concat("=").concat("application/json");


//			p = OAuthEncoder.encode("content-type").concat("=").concat("application/json");
//			params.add(p);
//			p = OAuthEncoder.encode("content-length").concat("=").concat("18");
//			params.add(p);

//			MOut.temp(p, params);
////			p = OAuthEncoder.encode("Content-length").concat("=").concat("18");
////			params.add(p);
//
////			String p2 = OAuthEncoder.encode("remarks").concat("=").concat("Foo1");
////			params.add(p2);
//	//		MOut.temp(params, p1, p2);
//
//		}

		Collections.sort( params );

		final StringBuilder builder = new StringBuilder();
		for( final String param : params )
			builder.append( '&' ).append( param );

		final String formUrlEncodedParams = OAuthEncoder.encode( builder.toString().substring( 1 ) );
		final String sanitizedURL = OAuthEncoder.encode( this.url.replaceAll( "\\?.*", "" ).replace( "\\:\\d{4}", "" ) );

		return String.format( "%s&%s&%s", this.verb, sanitizedURL, formUrlEncodedParams );
	}

	private String getNonce() {
		final Long ts = this.timer.getMilis();
//		return String.valueOf(ts + Math.abs(this.timer.getRandomInteger()));
		return String.valueOf( ts ) + String.valueOf( Math.abs( this.timer.getRandomInteger() ) );
//		return Lib_Random.getString(16, true, true, true, null);
	}

	private String getTimestampInSeconds() {
		final Long ts = this.timer.getMilis();
		return String.valueOf( TimeUnit.MILLISECONDS.toSeconds( ts ) );
	}

}
