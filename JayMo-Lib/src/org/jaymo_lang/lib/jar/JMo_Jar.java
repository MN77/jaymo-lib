/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.lib.jar;

import javax.swing.ImageIcon;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.sys.file.Lib_Jar;
import de.mn77.lib.graphic.I_ImageX;
import de.mn77.lib.graphic.MImageX;


/**
 * @author Michael Nitsche
 * @created 18.01.2022
 *
 *          TODO: JarReader, JarWriter?
 */
public class JMo_Jar extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
//			case "copyFile":
//				return this.mCopyFile(cr);
			case "image":
			case "readImage":
				return this.mReadImage( cr );

			default:
				return null;
		}
	}

//	private I_Object mCopyFile(CallRuntime cr) {
//		I_Object arg = cr.args(this, Str.class)[0];
//		String jarPath = Lib_Convert.getStringValue(cr, arg);
//		Lib_Jar.
//		return null;
//	}

	/**
	 * °image ^ readImage
	 * °readImage(Str jarPath)Image # Fetch an image from the jar path. The path must start with a leading slash '/'
	 */
	private JMo_Image mReadImage( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, JMo_Str.class )[0];
		final String jarPath = Lib_Convert.toStr( cr, arg ).rawString();

		try {
			final ImageIcon icon = Lib_Jar.getImageIcon( jarPath );
			final I_ImageX image = new MImageX( icon );
			return new JMo_Image( image );
		}
		catch( final Exception e ) {
			throw new ExternalError( cr, "Jar/Image access error", e.getMessage() );
		}
	}

}
